﻿using InformationSystem.Data;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class SoftSkillsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SoftSkillsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SoftSkills
        public async Task<IActionResult> Index()
        {
            return View(await _context.softSkills.ToListAsync());
        }

        // GET: SoftSkills/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.softSkills == null)
            {
                return NotFound();
            }

            var softSkills = await _context.softSkills
                .FirstOrDefaultAsync(m => m.SoftSkillsId == id);
            if (softSkills == null)
            {
                return NotFound();
            }

            return View(softSkills);
        }

        // GET: SoftSkills/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SoftSkills/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SoftSkillsId,CompetencyName,CompetencyDescriprion")] SoftSkills softSkills)
        {
            if (ModelState.IsValid)
            {
                _context.Add(softSkills);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(softSkills);
        }

        // GET: SoftSkills/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.softSkills == null)
            {
                return NotFound();
            }

            var softSkills = await _context.softSkills.FindAsync(id);
            if (softSkills == null)
            {
                return NotFound();
            }
            return View(softSkills);
        }

        // POST: SoftSkills/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SoftSkillsId,CompetencyName,CompetencyDescriprion")] SoftSkills softSkills)
        {
            if (id != softSkills.SoftSkillsId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(softSkills);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SoftSkillsExists(softSkills.SoftSkillsId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(softSkills);
        }

        // GET: SoftSkills/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.softSkills == null)
            {
                return NotFound();
            }

            var softSkills = await _context.softSkills
                .FirstOrDefaultAsync(m => m.SoftSkillsId == id);
            if (softSkills == null)
            {
                return NotFound();
            }

            return View(softSkills);
        }

        // POST: SoftSkills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.softSkills == null)
            {
                return Problem("Entity set 'ApplicationDbContext.softSkills'  is null.");
            }
            var softSkills = await _context.softSkills.FindAsync(id);
            if (softSkills != null)
            {
                _context.softSkills.Remove(softSkills);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SoftSkillsExists(int id)
        {
            return _context.softSkills.Any(e => e.SoftSkillsId == id);
        }
    }
}
