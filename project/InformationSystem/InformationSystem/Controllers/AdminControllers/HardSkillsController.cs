﻿using InformationSystem.Data;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class HardSkillsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HardSkillsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: HardSkills
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.hardSkills.Include(h => h.CompetencyCategories);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: HardSkills/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.hardSkills == null)
            {
                return NotFound();
            }

            var hardSkills = await _context.hardSkills
                .Include(h => h.CompetencyCategories)
                .FirstOrDefaultAsync(m => m.HardSkillsId == id);
            if (hardSkills == null)
            {
                return NotFound();
            }

            return View(hardSkills);
        }

        // GET: HardSkills/Create
        public IActionResult Create()
        {
            ViewData["CompetencyCategoriesId"] = new SelectList(_context.competencyCategories, "CompetencyCategoriesId", "CompetencyCategoriesId");
            return View();
        }

        // POST: HardSkills/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("HardSkillsId,CompetencyCategoriesId,CompetencyName,CompetencyDescription")] HardSkills hardSkills)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hardSkills);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetencyCategoriesId"] = new SelectList(_context.competencyCategories, "CompetencyCategoriesId", "CompetencyCategoriesId", hardSkills.CompetencyCategoriesId);
            return View(hardSkills);
        }

        // GET: HardSkills/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.hardSkills == null)
            {
                return NotFound();
            }

            var hardSkills = await _context.hardSkills.FindAsync(id);
            if (hardSkills == null)
            {
                return NotFound();
            }
            ViewData["CompetencyCategoriesId"] = new SelectList(_context.competencyCategories, "CompetencyCategoriesId", "CompetencyCategoriesId", hardSkills.CompetencyCategoriesId);
            return View(hardSkills);
        }

        // POST: HardSkills/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("HardSkillsId,CompetencyCategoriesId,CompetencyName,CompetencyDescription")] HardSkills hardSkills)
        {
            if (id != hardSkills.HardSkillsId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hardSkills);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HardSkillsExists(hardSkills.HardSkillsId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetencyCategoriesId"] = new SelectList(_context.competencyCategories, "CompetencyCategoriesId", "CompetencyCategoriesId", hardSkills.CompetencyCategoriesId);
            return View(hardSkills);
        }

        // GET: HardSkills/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.hardSkills == null)
            {
                return NotFound();
            }

            var hardSkills = await _context.hardSkills
                .Include(h => h.CompetencyCategories)
                .FirstOrDefaultAsync(m => m.HardSkillsId == id);
            if (hardSkills == null)
            {
                return NotFound();
            }

            return View(hardSkills);
        }

        // POST: HardSkills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.hardSkills == null)
            {
                return Problem("Entity set 'ApplicationDbContext.hardSkills'  is null.");
            }
            var hardSkills = await _context.hardSkills.FindAsync(id);
            if (hardSkills != null)
            {
                _context.hardSkills.Remove(hardSkills);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HardSkillsExists(int id)
        {
            return _context.hardSkills.Any(e => e.HardSkillsId == id);
        }
    }
}
