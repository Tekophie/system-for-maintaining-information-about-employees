﻿using InformationSystem.Data;
using InformationSystem.Data.Models.Project;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Net.WebSockets;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class ProjectListsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProjectListsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ProjectLists
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.projectList.Include(p => p.projects).Where(p => p.projects!=null);
            
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ProjectLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.projectList == null)
            {
                return NotFound();
            }

            var projectList = await _context.projectList
                .Include(p => p.projects)
                .FirstOrDefaultAsync(m => m.ProjectsListId == id);
            if (projectList == null)
            {
                return NotFound();
            }

            return View(projectList);
        }

        // GET: ProjectLists/Create
        public IActionResult Create()
        {
            ViewData["ProjectsId"] = new SelectList(_context.projects, "Id", "Name");
            return View();
        }

        // POST: ProjectLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProjectsListId,EmployeeId,ProjectsId,StartDate,EndDate,TechnologiesListId")] ProjectList projectList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(projectList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProjectsId"] = new SelectList(_context.projects, "Id", "Name", projectList.ProjectsId);
            return View(projectList);
        }

        // GET: ProjectLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.projectList == null)
            {
                return NotFound();
            }

            var projectList = await _context.projectList.FindAsync(id);
            if (projectList == null)
            {
                return NotFound();
            }
            ViewData["ProjectsId"] = new SelectList(_context.projects, "Id", "Name", projectList.ProjectsId);
            return View(projectList);
        }

        // POST: ProjectLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProjectsListId,EmployeeId,ProjectsId,StartDate,EndDate,TechnologiesListId")] ProjectList projectList)
        {
            if (id != projectList.ProjectsListId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(projectList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectListExists(projectList.ProjectsListId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProjectsId"] = new SelectList(_context.projects, "Id", "Name", projectList.ProjectsId);
            return View(projectList);
        }

        // GET: ProjectLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.projectList == null)
            {
                return NotFound();
            }

            var projectList = await _context.projectList
                .Include(p => p.projects)
                .FirstOrDefaultAsync(m => m.ProjectsListId == id);
            if (projectList == null)
            {
                return NotFound();
            }

            return View(projectList);
        }

        // POST: ProjectLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.projectList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.projectList'  is null.");
            }
            var projectList = await _context.projectList.FindAsync(id);
            if (projectList != null)
            {
                _context.projectList.Remove(projectList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProjectListExists(int id)
        {
            return _context.projectList.Any(e => e.ProjectsListId == id);
        }
    }
}
