﻿using InformationSystem.Data;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class CompetenceLevelsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CompetenceLevelsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: CompetenceLevels
        public async Task<IActionResult> Index()
        {
            return View(await _context.competenceLevel.ToListAsync());
        }

        // GET: CompetenceLevels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.competenceLevel == null)
            {
                return NotFound();
            }

            var competenceLevel = await _context.competenceLevel
                .FirstOrDefaultAsync(m => m.CompetenceLevelId == id);
            if (competenceLevel == null)
            {
                return NotFound();
            }

            return View(competenceLevel);
        }

        // GET: CompetenceLevels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CompetenceLevels/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CompetenceLevelId,Level")] CompetenceLevel competenceLevel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(competenceLevel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(competenceLevel);
        }

        // GET: CompetenceLevels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.competenceLevel == null)
            {
                return NotFound();
            }

            var competenceLevel = await _context.competenceLevel.FindAsync(id);
            if (competenceLevel == null)
            {
                return NotFound();
            }
            return View(competenceLevel);
        }

        // POST: CompetenceLevels/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CompetenceLevelId,Level")] CompetenceLevel competenceLevel)
        {
            if (id != competenceLevel.CompetenceLevelId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(competenceLevel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompetenceLevelExists(competenceLevel.CompetenceLevelId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(competenceLevel);
        }

        // GET: CompetenceLevels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.competenceLevel == null)
            {
                return NotFound();
            }

            var competenceLevel = await _context.competenceLevel
                .FirstOrDefaultAsync(m => m.CompetenceLevelId == id);
            if (competenceLevel == null)
            {
                return NotFound();
            }

            return View(competenceLevel);
        }

        // POST: CompetenceLevels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.competenceLevel == null)
            {
                return Problem("Entity set 'ApplicationDbContext.competenceLevel'  is null.");
            }
            var competenceLevel = await _context.competenceLevel.FindAsync(id);
            if (competenceLevel != null)
            {
                _context.competenceLevel.Remove(competenceLevel);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompetenceLevelExists(int id)
        {
            return _context.competenceLevel.Any(e => e.CompetenceLevelId == id);
        }
    }
}
