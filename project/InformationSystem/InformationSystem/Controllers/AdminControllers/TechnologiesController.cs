﻿using InformationSystem.Data;
using InformationSystem.Data.Models.Project;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class TechnologiesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TechnologiesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Technologies
        public async Task<IActionResult> Index()
        {
            return View(await _context.technologies.ToListAsync());
        }

        // GET: Technologies/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.technologies == null)
            {
                return NotFound();
            }

            var technologies = await _context.technologies
                .FirstOrDefaultAsync(m => m.TechnologiesId == id);
            if (technologies == null)
            {
                return NotFound();
            }

            return View(technologies);
        }

        // GET: Technologies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Technologies/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TechnologiesId,TechnologiesName")] Technologies technologies)
        {
            if (ModelState.IsValid)
            {
                _context.Add(technologies);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(technologies);
        }

        // GET: Technologies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.technologies == null)
            {
                return NotFound();
            }

            var technologies = await _context.technologies.FindAsync(id);
            if (technologies == null)
            {
                return NotFound();
            }
            return View(technologies);
        }

        // POST: Technologies/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TechnologiesId,TechnologiesName")] Technologies technologies)
        {
            if (id != technologies.TechnologiesId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(technologies);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TechnologiesExists(technologies.TechnologiesId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(technologies);
        }

        // GET: Technologies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.technologies == null)
            {
                return NotFound();
            }

            var technologies = await _context.technologies
                .FirstOrDefaultAsync(m => m.TechnologiesId == id);
            if (technologies == null)
            {
                return NotFound();
            }

            return View(technologies);
        }

        // POST: Technologies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.technologies == null)
            {
                return Problem("Entity set 'ApplicationDbContext.technologies'  is null.");
            }
            var technologies = await _context.technologies.FindAsync(id);
            if (technologies != null)
            {
                _context.technologies.Remove(technologies);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TechnologiesExists(int id)
        {
            return _context.technologies.Any(e => e.TechnologiesId == id);
        }
    }
}
