﻿using InformationSystem.Data;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class CompetencyCategoriesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CompetencyCategoriesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: CompetencyCategories
        public async Task<IActionResult> Index()
        {
            return View(await _context.competencyCategories.ToListAsync());
        }

        // GET: CompetencyCategories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.competencyCategories == null)
            {
                return NotFound();
            }

            var competencyCategories = await _context.competencyCategories
                .FirstOrDefaultAsync(m => m.CompetencyCategoriesId == id);
            if (competencyCategories == null)
            {
                return NotFound();
            }

            return View(competencyCategories);
        }

        // GET: CompetencyCategories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CompetencyCategories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CompetencyCategoriesId,CompetencyCategory")] CompetencyCategories competencyCategories)
        {
            if (ModelState.IsValid)
            {
                _context.Add(competencyCategories);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(competencyCategories);
        }

        // GET: CompetencyCategories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.competencyCategories == null)
            {
                return NotFound();
            }

            var competencyCategories = await _context.competencyCategories.FindAsync(id);
            if (competencyCategories == null)
            {
                return NotFound();
            }
            return View(competencyCategories);
        }

        // POST: CompetencyCategories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CompetencyCategoriesId,CompetencyCategory")] CompetencyCategories competencyCategories)
        {
            if (id != competencyCategories.CompetencyCategoriesId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(competencyCategories);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompetencyCategoriesExists(competencyCategories.CompetencyCategoriesId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(competencyCategories);
        }

        // GET: CompetencyCategories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.competencyCategories == null)
            {
                return NotFound();
            }

            var competencyCategories = await _context.competencyCategories
                .FirstOrDefaultAsync(m => m.CompetencyCategoriesId == id);
            if (competencyCategories == null)
            {
                return NotFound();
            }

            return View(competencyCategories);
        }

        // POST: CompetencyCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.competencyCategories == null)
            {
                return Problem("Entity set 'ApplicationDbContext.competencyCategories'  is null.");
            }
            var competencyCategories = await _context.competencyCategories.FindAsync(id);
            if (competencyCategories != null)
            {
                _context.competencyCategories.Remove(competencyCategories);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompetencyCategoriesExists(int id)
        {
            return _context.competencyCategories.Any(e => e.CompetencyCategoriesId == id);
        }
    }
}
