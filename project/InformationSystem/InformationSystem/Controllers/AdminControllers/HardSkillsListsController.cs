﻿using InformationSystem.Data;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class HardSkillsListsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HardSkillsListsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: HardSkillsLists
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.hardSkillsList.Include(h => h.CompetencеLevel).Include(h => h.hardSkill);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: HardSkillsLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.hardSkillsList == null)
            {
                return NotFound();
            }

            var hardSkillsList = await _context.hardSkillsList
                .Include(h => h.CompetencеLevel)
                .Include(h => h.hardSkill)
                .FirstOrDefaultAsync(m => m.HardSkillListId == id);
            if (hardSkillsList == null)
            {
                return NotFound();
            }

            return View(hardSkillsList);
        }

        // GET: HardSkillsLists/Create
        public IActionResult Create()
        {
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId");
            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName");
            return View();
        }

        // POST: HardSkillsLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("HardSkillListId,EmployeeId,HardSkillsId,CompetenceLevelId,AttitudeToCompetence,DesireToDevelop")] HardSkillsList hardSkillsList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hardSkillsList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", hardSkillsList.CompetenceLevelId);
            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName", hardSkillsList.HardSkillsId);
            return View(hardSkillsList);
        }

        // GET: HardSkillsLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.hardSkillsList == null)
            {
                return NotFound();
            }

            var hardSkillsList = await _context.hardSkillsList.FindAsync(id);
            if (hardSkillsList == null)
            {
                return NotFound();
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", hardSkillsList.CompetenceLevelId);
            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName", hardSkillsList.HardSkillsId);
            return View(hardSkillsList);
        }

        // POST: HardSkillsLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("HardSkillListId,EmployeeId,HardSkillsId,CompetenceLevelId,AttitudeToCompetence,DesireToDevelop")] HardSkillsList hardSkillsList)
        {
            if (id != hardSkillsList.HardSkillListId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hardSkillsList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HardSkillsListExists(hardSkillsList.HardSkillListId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", hardSkillsList.CompetenceLevelId);
            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName", hardSkillsList.HardSkillsId);
            return View(hardSkillsList);
        }

        // GET: HardSkillsLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.hardSkillsList == null)
            {
                return NotFound();
            }

            var hardSkillsList = await _context.hardSkillsList
                .Include(h => h.CompetencеLevel)
                .Include(h => h.hardSkill)
                .FirstOrDefaultAsync(m => m.HardSkillListId == id);
            if (hardSkillsList == null)
            {
                return NotFound();
            }

            return View(hardSkillsList);
        }

        // POST: HardSkillsLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.hardSkillsList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.hardSkillsList'  is null.");
            }
            var hardSkillsList = await _context.hardSkillsList.FindAsync(id);
            if (hardSkillsList != null)
            {
                _context.hardSkillsList.Remove(hardSkillsList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HardSkillsListExists(int id)
        {
            return _context.hardSkillsList.Any(e => e.HardSkillListId == id);
        }
    }
}
