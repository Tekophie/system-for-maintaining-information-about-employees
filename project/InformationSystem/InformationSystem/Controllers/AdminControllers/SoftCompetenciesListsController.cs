﻿using InformationSystem.Data;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class SoftCompetenciesListsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SoftCompetenciesListsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SoftCompetenciesLists
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.softCompetenciesList.Include(s => s.CompetenceLevel).Include(s => s.SoftSkills);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: SoftCompetenciesLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.softCompetenciesList == null)
            {
                return NotFound();
            }

            var softCompetenciesList = await _context.softCompetenciesList
                .Include(s => s.CompetenceLevel)
                .Include(s => s.SoftSkills)
                .FirstOrDefaultAsync(m => m.SoftCompetenciesListId == id);
            if (softCompetenciesList == null)
            {
                return NotFound();
            }

            return View(softCompetenciesList);
        }

        // GET: SoftCompetenciesLists/Create
        public IActionResult Create()
        {
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId");
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName");
            return View();
        }

        // POST: SoftCompetenciesLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SoftCompetenciesListId,EducationId,SoftSkillsId,CompetenceLevelId")] SoftCompetenciesList softCompetenciesList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(softCompetenciesList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", softCompetenciesList.CompetenceLevelId);
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName", softCompetenciesList.SoftSkillsId);
            return View(softCompetenciesList);
        }

        // GET: SoftCompetenciesLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.softCompetenciesList == null)
            {
                return NotFound();
            }

            var softCompetenciesList = await _context.softCompetenciesList.FindAsync(id);
            if (softCompetenciesList == null)
            {
                return NotFound();
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", softCompetenciesList.CompetenceLevelId);
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName", softCompetenciesList.SoftSkillsId);
            return View(softCompetenciesList);
        }

        // POST: SoftCompetenciesLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SoftCompetenciesListId,EducationId,SoftSkillsId,CompetenceLevelId")] SoftCompetenciesList softCompetenciesList)
        {
            if (id != softCompetenciesList.SoftCompetenciesListId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(softCompetenciesList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SoftCompetenciesListExists(softCompetenciesList.SoftCompetenciesListId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", softCompetenciesList.CompetenceLevelId);
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName", softCompetenciesList.SoftSkillsId);
            return View(softCompetenciesList);
        }

        // GET: SoftCompetenciesLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.softCompetenciesList == null)
            {
                return NotFound();
            }

            var softCompetenciesList = await _context.softCompetenciesList
                .Include(s => s.CompetenceLevel)
                .Include(s => s.SoftSkills)
                .FirstOrDefaultAsync(m => m.SoftCompetenciesListId == id);
            if (softCompetenciesList == null)
            {
                return NotFound();
            }

            return View(softCompetenciesList);
        }

        // POST: SoftCompetenciesLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.softCompetenciesList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.softCompetenciesList'  is null.");
            }
            var softCompetenciesList = await _context.softCompetenciesList.FindAsync(id);
            if (softCompetenciesList != null)
            {
                _context.softCompetenciesList.Remove(softCompetenciesList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SoftCompetenciesListExists(int id)
        {
            return _context.softCompetenciesList.Any(e => e.SoftCompetenciesListId == id);
        }
    }
}
