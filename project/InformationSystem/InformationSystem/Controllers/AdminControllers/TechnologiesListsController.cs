﻿using InformationSystem.Data;
using InformationSystem.Data.Models.Project;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class TechnologiesListsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TechnologiesListsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: TechnologiesLists
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.technologiesLists.Include(t => t.projectList).Include(t => t.technologies);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: TechnologiesLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.technologiesLists == null)
            {
                return NotFound();
            }

            var technologiesList = await _context.technologiesLists
                .Include(t => t.projectList)
                .Include(t => t.technologies)
                .FirstOrDefaultAsync(m => m.TechnologiesListId == id);

            if (technologiesList == null)
            {
                return NotFound();
            }

            return View(technologiesList);
        }

        // GET: TechnologiesLists/Create
        public IActionResult Create()
        {
            ViewData["ProjectListId"] = new SelectList(_context.projectList, "ProjectsListId", "ProjectsListId");
            ViewData["TechnologiesId"] = new SelectList(_context.technologies, "TechnologiesId", "TechnologiesName");
            return View();
        }

        // POST: TechnologiesLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TechnologiesListId,ProjectListId,EmployeeId,TechnologiesId")] TechnologiesList technologiesList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(technologiesList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProjectListId"] = new SelectList(_context.projectList, "ProjectsListId", "ProjectsListId", technologiesList.ProjectListId);
            ViewData["TechnologiesId"] = new SelectList(_context.technologies, "TechnologiesId", "TechnologiesName", technologiesList.TechnologiesId);
            return View(technologiesList);
        }

        // GET: TechnologiesLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.technologiesLists == null)
            {
                return NotFound();
            }

            var technologiesList = await _context.technologiesLists.FindAsync(id);
            if (technologiesList == null)
            {
                return NotFound();
            }
            ViewData["ProjectListId"] = new SelectList(_context.projectList, "ProjectsListId", "ProjectsListId", technologiesList.ProjectListId);
            ViewData["TechnologiesId"] = new SelectList(_context.technologies, "TechnologiesId", "TechnologiesName", technologiesList.TechnologiesId);
            return View(technologiesList);
        }

        // POST: TechnologiesLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TechnologiesListId,ProjectListId,EmployeeId,TechnologiesId")] TechnologiesList technologiesList)
        {
            if (id != technologiesList.TechnologiesListId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(technologiesList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TechnologiesListExists(technologiesList.TechnologiesListId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProjectListId"] = new SelectList(_context.projectList, "ProjectsListId", "ProjectsListId", technologiesList.ProjectListId);
            ViewData["TechnologiesId"] = new SelectList(_context.technologies, "TechnologiesId", "TechnologiesName", technologiesList.TechnologiesId);
            return View(technologiesList);
        }

        // GET: TechnologiesLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.technologiesLists == null)
            {
                return NotFound();
            }

            var technologiesList = await _context.technologiesLists
                .Include(t => t.projectList)
                .Include(t => t.technologies)
                .FirstOrDefaultAsync(m => m.TechnologiesListId == id);
            if (technologiesList == null)
            {
                return NotFound();
            }

            return View(technologiesList);
        }

        // POST: TechnologiesLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.technologiesLists == null)
            {
                return Problem("Entity set 'ApplicationDbContext.technologiesLists'  is null.");
            }
            var technologiesList = await _context.technologiesLists.FindAsync(id);
            if (technologiesList != null)
            {
                _context.technologiesLists.Remove(technologiesList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TechnologiesListExists(int id)
        {
            return _context.technologiesLists.Any(e => e.TechnologiesListId == id);
        }
    }
}
