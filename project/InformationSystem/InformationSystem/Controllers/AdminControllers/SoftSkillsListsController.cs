﻿using InformationSystem.Data;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class SoftSkillsListsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SoftSkillsListsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SoftSkillsLists
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.softSkillsList.Include(s => s.CompetencеLevel).Include(s => s.SoftSkills);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: SoftSkillsLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.softSkillsList == null)
            {
                return NotFound();
            }

            var softSkillsList = await _context.softSkillsList
                .Include(s => s.CompetencеLevel)
                .Include(s => s.SoftSkills)
                .FirstOrDefaultAsync(m => m.SoftSkillsListId == id);
            if (softSkillsList == null)
            {
                return NotFound();
            }

            return View(softSkillsList);
        }

        // GET: SoftSkillsLists/Create
        public IActionResult Create()
        {
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId");
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName");
            return View();
        }

        // POST: SoftSkillsLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SoftSkillsListId,EmployeeId,SoftSkillsId,CompetenceLevelId,DesireToDevelop")] SoftSkillsList softSkillsList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(softSkillsList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", softSkillsList.CompetenceLevelId);
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName", softSkillsList.SoftSkillsId);
            return View(softSkillsList);
        }

        // GET: SoftSkillsLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.softSkillsList == null)
            {
                return NotFound();
            }

            var softSkillsList = await _context.softSkillsList.FindAsync(id);
            if (softSkillsList == null)
            {
                return NotFound();
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", softSkillsList.CompetenceLevelId);
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName", softSkillsList.SoftSkillsId);
            return View(softSkillsList);
        }

        // POST: SoftSkillsLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SoftSkillsListId,EmployeeId,SoftSkillsId,CompetenceLevelId,DesireToDevelop")] SoftSkillsList softSkillsList)
        {
            if (id != softSkillsList.SoftSkillsListId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(softSkillsList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SoftSkillsListExists(softSkillsList.SoftSkillsListId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", softSkillsList.CompetenceLevelId);
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName", softSkillsList.SoftSkillsId);
            return View(softSkillsList);
        }

        // GET: SoftSkillsLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.softSkillsList == null)
            {
                return NotFound();
            }

            var softSkillsList = await _context.softSkillsList
                .Include(s => s.CompetencеLevel)
                .Include(s => s.SoftSkills)
                .FirstOrDefaultAsync(m => m.SoftSkillsListId == id);
            if (softSkillsList == null)
            {
                return NotFound();
            }

            return View(softSkillsList);
        }

        // POST: SoftSkillsLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.softSkillsList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.softSkillsList'  is null.");
            }
            var softSkillsList = await _context.softSkillsList.FindAsync(id);
            if (softSkillsList != null)
            {
                _context.softSkillsList.Remove(softSkillsList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SoftSkillsListExists(int id)
        {
            return _context.softSkillsList.Any(e => e.SoftSkillsListId == id);
        }
    }
}
