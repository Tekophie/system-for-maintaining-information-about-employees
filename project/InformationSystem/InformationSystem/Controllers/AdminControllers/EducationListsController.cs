﻿using InformationSystem.Data;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class EducationListsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EducationListsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: EducationLists
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.educationList.Include(e => e.education);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: EducationLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.educationList == null)
            {
                return NotFound();
            }

            var educationList = await _context.educationList
                .Include(e => e.education)
                .FirstOrDefaultAsync(m => m.EducationListId == id);
            if (educationList == null)
            {
                return NotFound();
            }

            return View(educationList);
        }

        // GET: EducationLists/Create
        public IActionResult Create()
        {
            ViewData["EducationId"] = new SelectList(_context.education, "EducationId", "CourseName");
            return View();
        }

        // POST: EducationLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EducationListId,EmployeeId,EducationId,PassageDateStart,PassageDateFinish,Certificate,Comment")] EducationList educationList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(educationList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EducationId"] = new SelectList(_context.education, "EducationId", "CourseName", educationList.EducationId);
            return View(educationList);
        }

        // GET: EducationLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.educationList == null)
            {
                return NotFound();
            }

            var educationList = await _context.educationList.FindAsync(id);
            if (educationList == null)
            {
                return NotFound();
            }
            ViewData["EducationId"] = new SelectList(_context.education, "EducationId", "CourseName", educationList.EducationId);
            return View(educationList);
        }

        // POST: EducationLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EducationListId,EmployeeId,EducationId,PassageDateStart,PassageDateFinish,Certificate,Comment")] EducationList educationList)
        {
            if (id != educationList.EducationListId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(educationList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EducationListExists(educationList.EducationListId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EducationId"] = new SelectList(_context.education, "EducationId", "CourseName", educationList.EducationId);
            return View(educationList);
        }

        // GET: EducationLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.educationList == null)
            {
                return NotFound();
            }

            var educationList = await _context.educationList
                .Include(e => e.education)
                .FirstOrDefaultAsync(m => m.EducationListId == id);
            if (educationList == null)
            {
                return NotFound();
            }

            return View(educationList);
        }

        // POST: EducationLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.educationList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.educationList'  is null.");
            }
            var educationList = await _context.educationList.FindAsync(id);
            if (educationList != null)
            {
                _context.educationList.Remove(educationList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EducationListExists(int id)
        {
            return _context.educationList.Any(e => e.EducationListId == id);
        }
    }
}
