﻿using InformationSystem.Data;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.AdminControllers
{
    [Authorize(Roles = "Admin")]
    public class HardCompetenciesListsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HardCompetenciesListsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: HardCompetenciesLists
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.hardCompetenciesList.Include(h => h.CompetenceLevel).Include(h => h.HardSkills);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: HardCompetenciesLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.hardCompetenciesList == null)
            {
                return NotFound();
            }

            var hardCompetenciesList = await _context.hardCompetenciesList
                .Include(h => h.CompetenceLevel)
                .Include(h => h.HardSkills)
                .FirstOrDefaultAsync(m => m.HardCompetenciesListId == id);
            if (hardCompetenciesList == null)
            {
                return NotFound();
            }

            return View(hardCompetenciesList);
        }

        // GET: HardCompetenciesLists/Create
        public IActionResult Create()
        {
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId");
            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName");
            return View();
        }

        // POST: HardCompetenciesLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("HardCompetenciesListId,EducationId,HardSkillsId,CompetenceLevelId")] HardCompetenciesList hardCompetenciesList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hardCompetenciesList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", hardCompetenciesList.CompetenceLevelId);
            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName", hardCompetenciesList.HardSkillsId);
            return View(hardCompetenciesList);
        }

        // GET: HardCompetenciesLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.hardCompetenciesList == null)
            {
                return NotFound();
            }

            var hardCompetenciesList = await _context.hardCompetenciesList.FindAsync(id);
            if (hardCompetenciesList == null)
            {
                return NotFound();
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", hardCompetenciesList.CompetenceLevelId);
            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName", hardCompetenciesList.HardSkillsId);
            return View(hardCompetenciesList);
        }

        // POST: HardCompetenciesLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("HardCompetenciesListId,EducationId,HardSkillsId,CompetenceLevelId")] HardCompetenciesList hardCompetenciesList)
        {
            if (id != hardCompetenciesList.HardCompetenciesListId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hardCompetenciesList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HardCompetenciesListExists(hardCompetenciesList.HardCompetenciesListId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "CompetenceLevelId", hardCompetenciesList.CompetenceLevelId);
            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName", hardCompetenciesList.HardSkillsId);
            return View(hardCompetenciesList);
        }

        // GET: HardCompetenciesLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.hardCompetenciesList == null)
            {
                return NotFound();
            }

            var hardCompetenciesList = await _context.hardCompetenciesList
                .Include(h => h.CompetenceLevel)
                .Include(h => h.HardSkills)
                .FirstOrDefaultAsync(m => m.HardCompetenciesListId == id);
            if (hardCompetenciesList == null)
            {
                return NotFound();
            }

            return View(hardCompetenciesList);
        }

        // POST: HardCompetenciesLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.hardCompetenciesList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.hardCompetenciesList'  is null.");
            }
            var hardCompetenciesList = await _context.hardCompetenciesList.FindAsync(id);
            if (hardCompetenciesList != null)
            {
                _context.hardCompetenciesList.Remove(hardCompetenciesList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HardCompetenciesListExists(int id)
        {
            return _context.hardCompetenciesList.Any(e => e.HardCompetenciesListId == id);
        }
    }
}
