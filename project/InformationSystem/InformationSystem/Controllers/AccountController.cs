﻿using InformationSystem.Data;
using InformationSystem.Data.Models;
using InformationSystem.ViewModels.Identity;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MimeKit;


namespace InformationSystem.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ApplicationDbContext _context;
        IConfiguration _configuration;

        public AccountController(ApplicationDbContext context, UserManager<User> userManager, SignInManager<User> signInManager, IConfiguration configuration)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {

                User user = new User { Email = model.Email, UserName = model.Email, Name = model.Name, Surname = model.Surname, EmailConfirmed = false, TwoFactorEnabled = true };
                // добавляем пользователя
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    // генерация токена для пользователя
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Action(
                        "ConfirmEmail",
                        "Account",
                        new { userId = user.Id, code = code },
                        protocol: HttpContext.Request.Scheme);
                    await SendEmailAsync(model.Email, "Подтверждение аккаунта",
                        $"Подтвердите регистрацию, перейдя по ссылке:<br> <a href='{callbackUrl}'>link</a>");

                    ModelState.AddModelError("", "Для завершения регистрации проверьте электронную почту и перейдите по ссылке, указанной в письме");

                    // установка куки
                    //await _signInManager.SignInAsync(user, false);
                    //return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(model);
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Информационная система", "inform4tion.system@gmail.com"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;

            string mailText = BuildTemplate("Controllers/", "Template.html");

            mailText = mailText.Replace("[Message]", message);
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = mailText
            };


            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 465, true);
                await client.AuthenticateAsync(_configuration["smtpEmail"], _configuration["smtpPassword"]);
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }

        public static string BuildTemplate(string path, string template)

        {

            StreamReader str = new StreamReader(Path.Combine(path, template));
            string mailText = str.ReadToEnd();
            str.Close();


            return mailText;

        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View("Error");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
                return RedirectToAction("Index", "Home");
            else
                return View("Error");
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result =
                    await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

                if (result.Succeeded)
                {
                    // проверяем, принадлежит ли URL приложению
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    var user = await _userManager.FindByNameAsync(model.Email);
                    if (user != null)
                    {
                        if (!await _userManager.IsEmailConfirmedAsync(user))
                        {
                            ModelState.AddModelError(string.Empty, "Вы не подтвердили свой email");
                            return View(model);
                        }
                        else
                        {
                            ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Пользователь не найден");
                    }

                }

                if (result.RequiresTwoFactor)
                {
                    return base.RedirectToAction(nameof(ViewModels.Identity.LoginTwoStep), new { model.Email, model.RememberMe, model.ReturnUrl });
                }

            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> LoginTwoStep(string email, bool rememberMe, string returnUrl = null)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return View("Error");
            }
            var providers = await _userManager.GetValidTwoFactorProvidersAsync(user);
            if (!providers.Contains("Email"))
            {
                return View("Error");
            }
            var token = await _userManager.GenerateTwoFactorTokenAsync(user, "Email");
            await SendEmailAsync(email, $"Подтвердите вход:", $"<div style='text-align: center; font-size:48px;'>{token}</div>");
            ViewData["ReturnUrl"] = returnUrl;
            return View(new LoginTwoStep { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginTwoStep(LoginTwoStep twoStepModel, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                return View(twoStepModel);
            }
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var result = await _signInManager.TwoFactorSignInAsync("Email", twoStepModel.TwoFactorCode, twoStepModel.RememberMe, rememberClient: false);
            if (result.Succeeded)
            {
                // проверяем, принадлежит ли URL приложению
                if (!string.IsNullOrEmpty(twoStepModel.ReturnUrl) && Url.IsLocalUrl(twoStepModel.ReturnUrl))
                {
                    return Redirect(twoStepModel.ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else if (result.IsLockedOut)
            {
                ModelState.AddModelError("", "Аккаунт заблокирован");
                return View();
            }
            else
            {
                ModelState.AddModelError("", "Ошибка авторизации");
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            // удаляем аутентификационные куки
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }


        public async Task<IActionResult> Lk()
        {

            return View();
        }

        public async Task<IActionResult> ChangePassword()
        {
            var user = await _userManager.GetUserAsync(User); if (user == null) return RedirectToAction("Login", "Account");

            ChangePasswordViewModel model = new ChangePasswordViewModel { Id = user.Id, Email = user.Email };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User); 
                if (user == null) return RedirectToAction("Login", "Account");

                if (user != null)
                {
                    IdentityResult result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Courses", "Course");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Пользователь не найден");
                }
            }
            return View(model);
        }

        public async Task<IActionResult> lkLoyoutAsync()
        {

            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");

            if (user == null)
            {
                return View("Error");
            }
            var userid = user.Id;


            ViewBag.userid = userid;


            return View();
        }


        public async Task<IActionResult> EditUser()
        {
            var user = await _userManager.GetUserAsync(User); if (user == null) return RedirectToAction("Login", "Account");
            EditUserViewModel model = new EditUserViewModel { Id = user.Id, Email = user.Email, Name = user.Name, Surname = user.Surname };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User); if (user == null) return RedirectToAction("Login", "Account");
                if (user != null)
                {
                    user.Name = model.Name;
                    user.Surname = model.Surname;

                    var result = await _userManager.UpdateAsync(user);

                    if (result.Succeeded)
                    {
                        return RedirectToAction("Courses", "Course");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
            }
            //return RedirectToAction("EditUser", "Account");
            return View(model);
        }

        public async Task<IActionResult> CheckCode()
        {
            return View();
        }
    }
}
