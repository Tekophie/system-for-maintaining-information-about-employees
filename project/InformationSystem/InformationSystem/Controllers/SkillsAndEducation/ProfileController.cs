﻿using InformationSystem.Data;
using InformationSystem.Data.Models;
using InformationSystem.ViewModels.Mentors;
using InformationSystem.ViewModels.Project;
using InformationSystem.ViewModels.QuarterlyPlans;
using InformationSystem.ViewModels.Skills;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.SkillsAndEducation
{
    public class ProfileController : Controller
    {
        private readonly ApplicationDbContext _context;
        Microsoft.AspNetCore.Identity.UserManager<User> _userManager;
        private readonly IMentors _mentors;

        private readonly IProject _project;

        private readonly IPlan _plan;

        public ProfileController(IProject project, ApplicationDbContext context, Microsoft.AspNetCore.Identity.UserManager<User> userManager, IMentors mentors, IPlan plan)
        {
            _context = context;
            _userManager = userManager;
            _mentors = mentors;
            _project = project;
            _plan = plan;

        }

        public IActionResult profileLayout()
        {
            return View();
        }

        public IActionResult CourseHardSkils()
        {

            return PartialView();
        }


        public async Task<IActionResult> profileView(string? id, string? email)
        {

            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");

            if (email != null)
            {
                var SearchBEEmail = await _userManager.FindByEmailAsync(email);
                if (SearchBEEmail != null) id = SearchBEEmail.Id; 
            }
            string userId = id;

            //возврат своей карточки
            if (id == null || id == user.Id)
            {
                userId = user.Id;
            }
            //проверка доступа
            else
            {
                // проверка роли пользователя
                bool isAdmin = User.IsInRole("Admin");
                bool isManager = User.IsInRole("Manager");
                if (isAdmin || isManager)
                {

                }
                //проверка наставника
                else if (_context.mentors.Where(p => p.MentorId == user.Id && p.TraineeId == userId
                                                        && p.TraineeConfirmed == true && p.MentorConfirmed == true).Any() == false)
                {
                    ModelState.AddModelError("", "Нет доступа к данному профилю");
                    return RedirectToAction("AccessDenied", "Home");
                }
            }


            //Проверка сущетсвует ли профиль
            if (_context.Users.Find(userId) == null)
            {
                return RedirectToAction("UserNotFound", "Home");
                //return Content("Пользователь не найден");
            }

            var userSearch = _context.Users.Find(userId);

            ViewBag.UserSurname = userSearch.Surname;
            ViewBag.UserName = userSearch.Name;
            ViewBag.Email = userSearch.Email;


            //количество элементов у ссылок
            ViewBag.CourseCount = await _context.educationList.Where(p => p.EmployeeId == userId).CountAsync();
            ViewBag.ProjectCount = await _context.projectList.Include(p => p.projects)
                                                           .Where(p => p.EmployeeId == userId && p.ProjectsId != null).CountAsync();
            ViewBag.SkillsCount = await _context.hardSkillsList.Where(p => p.EmployeeId == userId)
                                                    .CountAsync() + await _context.softSkillsList.Where(p => p.EmployeeId == userId)
                                                    .CountAsync();
            ViewBag.MentorsCount = await _context.mentors.Where(p => p.TraineeId == userId
                                                            && p.TraineeConfirmed == true
                                                            && p.MentorConfirmed == true).CountAsync();
            ViewBag.TraineeCount = await _context.mentors.Where(p => p.MentorId == userId
                                                            && p.TraineeConfirmed == true
                                                            && p.MentorConfirmed == true).CountAsync();
            ViewBag.PlanCount = await _context.quarterlyDevelopmentPlanList.Include(p => p.QuarterlyDevelopmentPlan)
                                                           .Where(p => p.EmployeeId == userId && p.QuarterlyDevelopmentPlan.Objective.Length > 0).CountAsync();

            //данные пользователя
            //курсы
            var buf = await _context.educationList.Where(p => p.EmployeeId == userId)
                                                          .Include(p => p.education).ToListAsync();
            List<CreateCourseViewModel> list = new List<CreateCourseViewModel>();
            foreach (var item in buf)
            {
                list.Add(new CreateCourseViewModel
                {
                    education = await _context.education.FindAsync(item.EducationId),
                    hardCompetenciesList = _context.hardCompetenciesList.Include(p => p.CompetenceLevel)
                                                                    .Include(p => p.HardSkills)
                                                                    .Where(p => p.EducationId == item.EducationId)
                                                                    .ToList(),
                    softCompetenciesList = _context.softCompetenciesList.Where(p => p.EducationId == item.EducationId)
                                                                    .Include(p => p.CompetenceLevel)
                                                                    .Include(p => p.SoftSkills)
                                                                    .ToList(),
                    educationList = item,
                });
            }
            ViewBag.Courses = list;

            //проекты
            /*ViewBag.Projects = await _context.projectList.Where(p => p.EmployeeId== userId && p.ProjectsId != null && p.ProjectsId != null)
                                                         .Include(o => o.projects)
                                                         .Include(o => o.technologiesLists) 
                                                         .ToListAsync();*/

            ViewBag.Projects = _project.GetProject(userId);

            ViewBag.Plans = _plan.GetPlan(userId);

            //ViewBag.Projects = null;

            ViewBag.HardSkills = await _context.hardSkillsList.Where(p => p.EmployeeId == userId)
                                                                .Include(p => p.hardSkill)
                                                                .Include(p => p.hardSkill.CompetencyCategories)
                                                                .Include(p => p.CompetencеLevel)
                                                                .ToListAsync();

            ViewBag.SoftSkills = await _context.softSkillsList.Where(p => p.EmployeeId == userId)
                                                                .Include(p => p.CompetencеLevel)
                                                                .Include(p => p.SoftSkills)
                                                                .ToListAsync();
            var usersMentors = _mentors.getMentors(userId, false);
            ViewBag.Mentors = usersMentors;

            ViewBag.Trainee = _mentors.getMentors(userId, true);



            //пользователи с доступом к карточке

            var g = await _context.UserRoles.Where(p => p.RoleId == "ca8d9346-02c6-4c88-be51-712d58bc467c"
                                                       || p.RoleId == "c9508e97-818f-4268-bd8c-794d422fb7d9")
                                            .ToListAsync();
            List<MentorViewModel> AccessUsers = new List<MentorViewModel>();

            var allUsers = _userManager.Users.ToList();

            foreach (var item in g)
            {
                var mentor = allUsers.FirstOrDefault(p => p.Id == item.UserId);
                AccessUsers.Add(
                    new MentorViewModel
                    {
                        Surname = mentor.Surname,
                        Name = mentor.Name,
                        Email = mentor.Email
                    });
            }

            AccessUsers.AddRange(usersMentors);
            AccessUsers = AccessUsers.DistinctBy(p => p.Email).ToList();
            ViewBag.AccessUsers = AccessUsers;


            var kkkk = await _context.logging.Where(p => p.EmployeeId == userId).OrderByDescending(p => p.Date).ToListAsync();

            ViewBag.Story = kkkk;

            return View();
        }

    }
}
