﻿using InformationSystem.Data;
using InformationSystem.Data.Models;
using InformationSystem.Data.Models.Project;
using InformationSystem.Data.Models.SkillsAndEducation;
using InformationSystem.ViewModels.Skills;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Build.ObjectModelRemoting;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.SkillsAndEducation
{
    public class ProjectController : Controller
    {
        private readonly ApplicationDbContext _context;
        Microsoft.AspNetCore.Identity.UserManager<User> _userManager;

        public ProjectController(ApplicationDbContext context, Microsoft.AspNetCore.Identity.UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");

            var applicationDbContext = await _context.projectList.Include(p => p.projects)
                                                                 .Where(p => p.EmployeeId == user.Id && p.ProjectsId != null)
                                                                 .ToListAsync();


            List<ProjectView> projects = new List<ProjectView>();
            foreach (var item in applicationDbContext)
            {
                projects.Add(new ProjectView
                {
                    projectList = item,
                    technologies = _context.technologiesLists.Where(p => p.ProjectListId == item.ProjectsListId)
                    .Include(l => l.technologies)
                    .ToList()

                });
            }

            return View(projects);

            //return View(applicationDbContext);
        }

        // GET: ProjectLists/Create
        public async Task<IActionResult> AddProject(int? id, bool edit)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");

            var projectList = new ProjectList();
            if (id == null || _context.projectList == null)
            {
                projectList.EmployeeId = user.Id;

                if (ModelState.IsValid)
                {
                    _context.Add(projectList);
                    await _context.SaveChangesAsync();
                    id = projectList.ProjectsListId;
                    //return RedirectToAction("AddCourse");*/
                    return RedirectToAction("AddProject", "Project", new { id = id });
                }
            }
            else
            {
                if (!_context.projectList.Any(e => e.ProjectsListId == id && e.EmployeeId == user.Id))
                {
                    return RedirectToAction("AccessDenied", "Home");
                }

                projectList = await _context.projectList.FindAsync(id);
                if (projectList == null)
                {
                    return NotFound();
                }
            }

            var obj = new ProjectListViewModel
            {
                projectList = projectList,
                technologiesLists = await _context.technologiesLists.Where(p => p.ProjectListId == id && p.EmployeeId == user.Id)
                                                                    .Include(p => p.technologies)
                                                                    .ToListAsync(),
            };

            ViewBag.Edit = false;
            if (edit == true)
            {
                ViewBag.Edit = edit;
            }
            
            ViewData["ProjectsId"] = new SelectList(_context.projects.Where(p => p.Name != string.Empty), "Id", "Name").Reverse();

            return View(obj);
        }

        // POST: ProjectLists/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddProject([Bind("ProjectsListId,EmployeeId,ProjectsId,StartDate,EndDate,Technologies")] ProjectList projectList, bool parameterEdit)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            
            if (!_context.projectList.Any(e => e.ProjectsListId == projectList.ProjectsListId && e.EmployeeId == user.Id))
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            projectList.EmployeeId = user.Id;
            projectList.ProjectsId = _context.projects.FirstOrDefault().Id;

            if (ModelState.IsValid)
            {
                if (projectList.EndDate < projectList.StartDate)
                {
                    ModelState.AddModelError("", "Проверьте даты начала и окончания проекта");
                }
                else
                {
                    try
                    {
                        //добавление лога
                        if(parameterEdit)
                        {
                            string ll = _context.projects.FirstOrDefault(p => p.Id == projectList.ProjectsId).Name;
                            Logging logging = new Logging
                            {
                                Date = DateTime.Now,
                                EmployeeId = user.Id,
                                Description = "Изменен проект " + ll,
                            };
                            _context.Add(logging);
                        }
                        else
                        {
                            string ll = _context.projects.FirstOrDefault(p => p.Id == projectList.ProjectsId).Name;
                            Logging logging = new Logging
                            {
                                Date = DateTime.Now,
                                EmployeeId = user.Id,
                                Description = "Добавлен проект " + ll,
                            };
                            _context.Add(logging);
                        }

                        _context.Update(projectList);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!ProjectsExists(projectList.ProjectsListId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction("Index", "Project");
                }

            }

            return RedirectToAction("AddProject", "Project", new { id = projectList.ProjectsListId });
        }

        // GET: Projects/Create
        public async Task<IActionResult> CreateNewProject(int? id)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");

            var projects = new Projects();
            if (id == null || _context.projects == null)
            {
                projects.Name = string.Empty;
                projects.UserId = user.Id;
                if (ModelState.IsValid)
                {
                    _context.Add(projects);
                    await _context.SaveChangesAsync();
                    id = projects.Id;
                    //return RedirectToAction("AddCourse");
                    return RedirectToAction("CreateNewProject", "Project", new { id = id });
                }
            }
            else
            {
                projects = await _context.projects.FindAsync(id);
                if (projects == null)
                {
                    return NotFound();
                }
            }

            var obj = new ProjectViewModel1
            {
                projects = projects,
                tasks = await _context.tasks.Include(p => p.Projects)
                                            .Where(p => p.ProjectsId == projects.Id)
                                            .ToListAsync(),
            };
            return View(obj);
        }

        // POST: Projects/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateNewProject(int id, [Bind("Id,Name,Description,UserId")] Projects projects)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            projects.UserId = user.Id;


            if (id != projects.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {

                try
                {
                    _context.Update(projects);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectsExists(projects.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("AddProject", "Project");
            }

            return View(projects);

        }

        private bool ProjectsExists(int id)
        {
            return _context.projects.Any(e => e.Id == id);
        }

        // GET: TechnologiesLists/Create
        public async Task<IActionResult> AddTechologyToProject(int? project)
        {
            ViewData["TechnologiesId"] = new SelectList(_context.technologies, "TechnologiesId", "TechnologiesName").Reverse();
            ViewData["ProjectId"] = project;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddTechologyToProject([Bind("TechnologiesListId,ProjectListId, EmployeeId, TechnologiesId")] TechnologiesList technologiesList)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            technologiesList.EmployeeId = user.Id;

            if (ModelState.IsValid)
            {
                if (_context.technologiesLists.Where(p => p.EmployeeId == user.Id
                                                        && p.TechnologiesId == technologiesList.TechnologiesId
                                                        && p.ProjectListId == technologiesList.ProjectListId).Any())
                {
                    ModelState.AddModelError("", "Выбранная технология уже добавлена");
                }
                else
                {
                    int? ll = _context.projectList.Find(technologiesList.ProjectListId).ProjectsId;
                    string projectName = _context.projects.Find(ll).Name;

                    Logging logging = new Logging
                    {
                        Date = DateTime.Now,
                        EmployeeId = user.Id,
                        Description = "Внесены изменения в проект " + projectName,
                    };
                    _context.Add(logging);

                    _context.Add(technologiesList);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("AddProject", "Project", new { id = technologiesList.ProjectListId });
                }
            }

            ViewData["TechnologiesId"] = new SelectList(_context.technologies, "TechnologiesId", "TechnologiesName").Reverse();
            ViewData["ProjectId"] = technologiesList.ProjectListId;

            return View();
        }


        // POST: TechnologiesLists/Delete/5
        public async Task<IActionResult> RemoveTechnologyFromProject(int id, int? project)
        {

            if (_context.technologiesLists == null)
            {
                return Problem("Entity set 'ApplicationDbContext.technologiesLists'  is null.");
            }

            var technologiesList = await _context.technologiesLists.FindAsync(id);
            if (technologiesList != null)
            {
                _context.technologiesLists.Remove(technologiesList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction("AddProject", "Project", new { id = technologiesList.ProjectListId });

        }



        // GET: Tasks/Create
        public IActionResult AddTaskToProject(int? project)
        {
            ViewBag.Project = project;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddTaskToProject([Bind("TasksId,ProjectsId,TaskName")] Tasks tasks)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tasks);
                await _context.SaveChangesAsync();
                return RedirectToAction("CreateNewProject", "Project", new { id = tasks.ProjectsId });
            }

            ViewBag.Project = tasks.ProjectsId;
            return View(tasks);
        }


        public async Task<IActionResult> RemoveTaskFromProject(int id)
        {
            if (_context.tasks == null)
            {
                return Problem("Entity set 'ApplicationDbContext.tasks'  is null.");
            }
            var tasks = await _context.tasks.FindAsync(id);
            if (tasks != null)
            {
                _context.tasks.Remove(tasks);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction("CreateNewProject", "Project", new { id = tasks.ProjectsId });
        }

        public async Task<IActionResult> DeleteProject(int? id)
        {
            if (id == null || _context.projectList == null)
            {
                return NotFound();
            }

            var projectList = await _context.projectList.FindAsync(id);

            if (projectList == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            if (!_context.projectList.Any(e => e.ProjectsListId == id && e.EmployeeId == user.Id))
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            if (projectList != null)
            {
                var buf = _context.technologiesLists.Where(l => l.ProjectListId == id).ToList();
                _context.technologiesLists.RemoveRange(buf);

                string ll = _context.projects.FirstOrDefault(p => p.Id == projectList.ProjectsId).Name;
                Logging logging = new Logging
                {
                    Date = DateTime.Now,
                    EmployeeId = user.Id,
                    Description = "Удален проект " + ll,
                };
                _context.Add(logging);

                _context.projectList.Remove(projectList);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }



        public async Task<IActionResult> CreateNewTechnology(int? project)
        {
            if (project == null)
            {
                return NotFound();
            }

            ViewBag.Project = project;
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateNewTechnology(int project, [Bind("TechnologiesId,TechnologiesName")] Technologies technologies)
        {
            if (ModelState.IsValid)
            {
                _context.Add(technologies);
                await _context.SaveChangesAsync();
                return RedirectToAction("AddTechologyToProject", new { project = project });
            }
            ViewBag.Project = project;
            return PartialView(technologies);
        }

        public async Task<ActionResult> Change(EditProject model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var obj = new Projects();
                    obj = await _context.projects.FindAsync(model.id);

                    if (obj != null)
                    {
                        obj.Name = model.name;
                        obj.Description = model.description;
                        _context.Update(obj);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        return Json(false);
                    }

                    return Json(true);
                }
                catch (Exception)
                {
                    return Json(false);
                }
            }
            else
            {
                return Json(false);
            }
        }

        public async Task<ActionResult> ChangeProjectList(EditProjectList model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var obj = new ProjectList();

                    obj = await _context.projectList.FindAsync(model.id);
                    if (obj != null)
                    {
                        if(model.StartDate < model.EndDate)
                        {
                            obj.StartDate = model.StartDate;
                            obj.EndDate = model.EndDate;
                            obj.ProjectsId = model.projectid;
                            _context.Update(obj);
                            await _context.SaveChangesAsync();
                        }
                        else
                        {
                            return Json(false);
                        }
                    }
                    else
                    {
                        return Json(false);
                    }

                    return Json(true);
                }
                catch (Exception)
                {
                    return Json(false);
                }
            }
            else
            {
                return Json(false);
            }
        }
    }

    public class ProjectView
    {
        public ProjectList? projectList { get; set; }

        public List<TechnologiesList> technologies { get; set; }
    }

    public class EditProject
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class EditProjectList
    {
        public int id { get; set; }
        public int projectid { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
