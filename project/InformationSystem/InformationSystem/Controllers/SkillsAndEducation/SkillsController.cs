﻿using InformationSystem.Data;
using InformationSystem.Data.Models;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.SkillsAndEducation
{
    public class SkillsController : Controller
    {
        private readonly ApplicationDbContext _context;
        Microsoft.AspNetCore.Identity.UserManager<User> _userManager;

        public SkillsController(ApplicationDbContext context, Microsoft.AspNetCore.Identity.UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> HardSkills()
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");

            var obj = await _context.hardSkillsList.Where(p => p.EmployeeId == user.Id)
                                                    .Include(p => p.hardSkill)
                                                    .Include(p => p.hardSkill.CompetencyCategories)
                                                    .Include(p => p.CompetencеLevel)
                                                    .ToListAsync();
            return View(obj);
        }

        public IActionResult Create()
        {
            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName");
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("HardSkillsId,EmployeeId,HardSkillsId,CompetenceLevelId,AttitudeToCompetence,DesireToDevelop")] HardSkillsList hardSkillsList)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            hardSkillsList.EmployeeId = user.Id;

            if (ModelState.IsValid)
            {

                if(!_context.hardSkillsList.Where(p => p.EmployeeId == user.Id && p.HardSkillsId == hardSkillsList.HardSkillsId).Any())
                {
                    string ll = _context.hardSkills.FirstOrDefault(p => p.HardSkillsId == hardSkillsList.HardSkillsId).CompetencyName;

                    Logging logging = new Logging
                    {
                        Date = DateTime.Now,
                        EmployeeId = user.Id,
                        Description = "Добавлен навык " + ll,
                    };
                    _context.Add(logging);

                    _context.Add(hardSkillsList);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("HardSkills");
                }
                else
                {
                    ModelState.AddModelError("", "Выбранный навык уже добавлен");
                }
            }

            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName");
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level");
            return View(hardSkillsList);
        }

        public IActionResult CreateHardSkill()
        {
            ViewData["CompetencyCategoriesId"] = new SelectList(_context.competencyCategories, "CompetencyCategoriesId", "CompetencyCategory");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateHardSkill([Bind("HardSkillsId,CompetencyCategoriesId,CompetencyName,CompetencyDescription")] HardSkills hardSkills)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hardSkills);
                await _context.SaveChangesAsync();
                return RedirectToAction("Create");
            }
            return View(hardSkills);
        }


        public async Task<IActionResult> SoftSkills()
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");

            var obj = await _context.softSkillsList.Where(p => p.EmployeeId == user.Id)
                                                    .Include(p => p.CompetencеLevel)
                                                    .Include(p => p.SoftSkills)
                                                    .ToListAsync();

            return View(obj);
        }

        public IActionResult CreateSoft()
        {
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName");
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateSoft([Bind("SoftSkillsListId,EmployeeId,SoftSkillsId,CompetenceLevelId,DesireToDevelop")] SoftSkillsList softSkillsList)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            softSkillsList.EmployeeId = user.Id;

            if (ModelState.IsValid)
            {
                if (!_context.softSkillsList.Where(p => p.EmployeeId == user.Id && p.SoftSkillsId == softSkillsList.SoftSkillsId).Any())
                {
                    string ll = _context.softSkills.FirstOrDefault(p => p.SoftSkillsId == softSkillsList.SoftSkillsId).CompetencyName;

                    Logging logging = new Logging
                    {
                        Date = DateTime.Now,
                        EmployeeId = user.Id,
                        Description = "Добавлено качество " + ll,
                    };
                    _context.Add(logging);

                    _context.Add(softSkillsList);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("SoftSkills");
                }
                else
                {
                    ModelState.AddModelError("", "Выбраное качество уже добавлено");
                }
            }
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName");
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level");
            return View(softSkillsList);
        }

        // GET: SoftSkills/Create
        public IActionResult CreateSoftSkill()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateSoftSkill([Bind("SoftSkillsId,CompetencyName,CompetencyDescriprion")] SoftSkills softSkills)
        {
            if (ModelState.IsValid)
            {
                _context.Add(softSkills);
                await _context.SaveChangesAsync();
                return RedirectToAction("CreateSoft");
            }
            return View(softSkills);
        }

        public async Task<IActionResult> EditHard(int? id)
        {
            if (id == null || _context.hardSkillsList == null)
            {
                return NotFound();
            }

            var hardSkillsList = await _context.hardSkillsList.FindAsync(id);
            if (hardSkillsList == null)
            {
                return NotFound();
            }
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            if (!_context.hardSkillsList.Any(e => e.HardSkillListId == hardSkillsList.HardSkillListId && e.EmployeeId == user.Id))
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level", hardSkillsList.CompetenceLevelId);
            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName", hardSkillsList.HardSkillsId);
            return View(hardSkillsList);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditHard(int id, [Bind("HardSkillListId,EmployeeId,HardSkillsId,CompetenceLevelId,AttitudeToCompetence,DesireToDevelop")] HardSkillsList hardSkillsList)
        {
            if (id != hardSkillsList.HardSkillListId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
                if (!_context.hardSkillsList.Any(e => e.HardSkillListId == hardSkillsList.HardSkillListId && e.EmployeeId == user.Id))
                {
                    return RedirectToAction("AccessDenied", "Home");
                }

                try
                {
                    string ll = _context.hardSkills.FirstOrDefault(p => p.HardSkillsId == hardSkillsList.HardSkillsId).CompetencyName;

                    Logging logging = new Logging
                    {
                        Date = DateTime.Now,
                        EmployeeId = user.Id,
                        Description = "Изменен навык " + ll,
                    };
                    _context.Add(logging);

                    _context.Update(hardSkillsList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.hardSkillsList.Any(e => e.HardSkillListId == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("HardSkills");
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level", hardSkillsList.CompetenceLevelId);
            ViewData["HardSkillsId"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName", hardSkillsList.HardSkillsId);
            return View(hardSkillsList);
        }

        public async Task<IActionResult> DeleteHard(int? id)
        {
            if (id == null || _context.hardSkillsList == null)
            {
                return NotFound();
            }

            var hardSkillsList = await _context.hardSkillsList.FindAsync(id);
            if (hardSkillsList == null)
            {
                return NotFound();
            }

            if (_context.hardSkillsList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.hardSkillsList'  is null.");
            }
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            if (!_context.hardSkillsList.Any(e => e.HardSkillListId == hardSkillsList.HardSkillListId && e.EmployeeId == user.Id))
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            if (hardSkillsList != null)
            {
                string ll = _context.hardSkills.FirstOrDefault(p => p.HardSkillsId == hardSkillsList.HardSkillsId).CompetencyName;

                Logging logging = new Logging
                {
                    Date = DateTime.Now,
                    EmployeeId = user.Id,
                    Description = "Удален навык " + ll,
                };
                _context.Add(logging);
                _context.hardSkillsList.Remove(hardSkillsList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction("HardSkills");
        }


        public async Task<IActionResult> EditSoft(int? id)
        {
            if (id == null || _context.softSkillsList == null)
            {
                return NotFound();
            }

            var softSkillsList = await _context.softSkillsList.FindAsync(id);
            if (softSkillsList == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            if (!_context.softSkillsList.Any(e => e.SoftSkillsListId == softSkillsList.SoftSkillsListId && e.EmployeeId == user.Id))
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level", softSkillsList.CompetenceLevelId);
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName", softSkillsList.SoftSkillsId);
            return View(softSkillsList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditSoft(int id, [Bind("SoftSkillsListId,EmployeeId,SoftSkillsId,CompetenceLevelId,DesireToDevelop")] SoftSkillsList softSkillsList)
        {
            if (id != softSkillsList.SoftSkillsListId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
                    if (!_context.softSkillsList.Any(e => e.SoftSkillsListId == softSkillsList.SoftSkillsListId && e.EmployeeId == user.Id))
                    {
                        return RedirectToAction("AccessDenied", "Home");
                    }
                    softSkillsList.EmployeeId = user.Id;

                    var ll = await _context.softSkillsList.Include(p => p.SoftSkills).FirstOrDefaultAsync(p => p.SoftSkillsListId == softSkillsList.SoftSkillsListId);
                    ll.CompetenceLevelId = softSkillsList.CompetenceLevelId;
                    ll.DesireToDevelop = softSkillsList.DesireToDevelop;

                    Logging logging = new Logging
                    {
                        Date = DateTime.Now,
                        EmployeeId = user.Id,
                        Description = "Изменено качество " + ll.SoftSkills.CompetencyName,
                    };
                    _context.Add(logging);

                    _context.Update(ll);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.softSkillsList.Any(e => e.SoftSkillsListId == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("SoftSkills");
            }

            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level", softSkillsList.CompetenceLevelId);
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName", softSkillsList.SoftSkillsId);
            return View(softSkillsList);


        }

        public async Task<IActionResult> DeleteSoft(int? id)
        {
            if (id == null || _context.softSkillsList == null)
            {
                return NotFound();
            }

            var softSkillsList = await _context.softSkillsList
                .Include(s => s.CompetencеLevel)
                .Include(s => s.SoftSkills)
                .FirstOrDefaultAsync(m => m.SoftSkillsListId == id);
            if (softSkillsList == null)
            {
                return NotFound();
            }

            if (_context.softSkillsList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.softSkillsList'  is null.");
            }

            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            if (!_context.softSkillsList.Any(e => e.SoftSkillsListId == softSkillsList.SoftSkillsListId && e.EmployeeId == user.Id))
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            softSkillsList = await _context.softSkillsList.FindAsync(id);
            if (softSkillsList != null)
            {
                string ll = _context.softSkills.FirstOrDefault(p => p.SoftSkillsId == softSkillsList.SoftSkillsId).CompetencyName;

                Logging logging = new Logging
                {
                    Date = DateTime.Now,
                    EmployeeId = user.Id,
                    Description = "Удалено качество " + ll,
                };
                _context.Add(logging);
                _context.softSkillsList.Remove(softSkillsList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction("SoftSkills");
        }
    }
}
