﻿using InformationSystem.Data;
using InformationSystem.Data.Models;
using InformationSystem.Data.Models.SkillsAndEducation;
using InformationSystem.ViewModels.Skills;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.SkillsAndEducation
{
    public class CourseController : Controller
    {
        private readonly ApplicationDbContext _context;
        UserManager<User> _userManager;

        public CourseController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Courses()
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            var obj = await _context.educationList.Where(p => p.EmployeeId == user.Id).Include(p => p.education).ToListAsync();
            return View(obj);
        }

        public IActionResult AddCourse()
        {
            ViewData["EducationId"] = new SelectList(_context.education.Where(p => p.CourseName != string.Empty), "EducationId", "CourseName").Reverse();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddCourse([Bind("EducationListId,EmployeeId,EducationId,PassageDateStart,PassageDateFinish,Certificate,Comment")] EducationList educationList)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            educationList.EmployeeId = user.Id;

            if (ModelState.IsValid)
            {
                if (educationList.PassageDateStart > educationList.PassageDateFinish)
                {
                    ModelState.AddModelError("", "Проверьте даты начала и окончания курса");
                }
                else
                {
                    string ll = _context.education.FirstOrDefault(p => p.EducationId == educationList.EducationId).CourseName;
                    Logging logging = new Logging{
                        Date = DateTime.Now,
                        EmployeeId=user.Id,
                        Description = "Добавлен курс " + ll,
                    };
                    _context.Add(logging);

                    _context.Add(educationList);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Courses");
                }
            }
            ViewData["EducationId"] = new SelectList(_context.education.Where(p => p.CourseName != string.Empty), "EducationId", "CourseName").Reverse();

            return View(educationList);
        }


        public async Task<IActionResult> CreateNewCourse(int? id)
        {
            var education = new Education();
            if (id == null || _context.education == null)
            {
                education.CourseName = string.Empty;
                if (ModelState.IsValid)
                {
                    _context.Add(education);
                    await _context.SaveChangesAsync();
                    id = education.EducationId;
                    return RedirectToAction("CreateNewCourse", "Course", new { id = id });
                }
            }
            else
            {

                education = await _context.education.FindAsync(id);
                if (education == null)
                {
                    return NotFound();
                }
            }

            var obj = new CreateCourseViewModel
            {
                education = education,
                hardSkills = _context.hardSkills.ToList(),
                hardCompetenciesList = _context.hardCompetenciesList.Include(p => p.CompetenceLevel)
                                                                    .Where(p => p.EducationId == education.EducationId)
                                                                    .ToList(),
                softSkills = _context.softSkills.ToList(),
                softCompetenciesList = _context.softCompetenciesList.Where(p => p.EducationId == education.EducationId)
                                                                    .Include(p => p.CompetenceLevel)
                                                                    .ToList(),
            };

            ViewData["HardSkills"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName");

            return View(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateNewCourse(int id, [Bind("EducationId,CourseName,SoftCompetenciesList,HardCompetenciesList")] Education education)
        {
            if (id != education.EducationId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(education);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EducationExists(education.EducationId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("AddCourse");
            }
            return View(education);
        }

        private bool EducationExists(int id)
        {
            return _context.education.Any(e => e.EducationId == id);
        }

        public IActionResult HardSkillList()
        {
            var obj = _context.hardCompetenciesList.ToListAsync();
            return PartialView(obj);
        }

        // GET: HardCompetenciesLists/Create
        public IActionResult AddHardCompetenceToCourse(int? course)
        {
            ViewData["HardSkills"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName");
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level");
            ViewBag.Course = course;
            //int? obj = course;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddHardCompetenceToCourse([Bind("HardCompetenciesListId,EducationId,HardSkillsId,CompetenceLevelId")] HardCompetenciesList hardCompetenciesList)
        {
            if (_context.hardCompetenciesList.Where(p => p.HardSkillsId == hardCompetenciesList.HardSkillsId && p.EducationId == hardCompetenciesList.EducationId).Any())
            {
                ModelState.AddModelError("", "Навык уже добавлен");
                ViewData["HardSkills"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName");
                ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level");
                ViewBag.Course = hardCompetenciesList.EducationId;
                return View(hardCompetenciesList);
            }

            if (ModelState.IsValid)
            {
                _context.Add(hardCompetenciesList);
                await _context.SaveChangesAsync();
                return RedirectToAction("CreateNewCourse", "Course", new { id = hardCompetenciesList.EducationId });
            }

            ViewBag.Course = hardCompetenciesList.EducationId;
            ViewData["HardSkills"] = new SelectList(_context.hardSkills, "HardSkillsId", "CompetencyName");
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level");
            return View(hardCompetenciesList);
        }

        public async Task<IActionResult> RemoveHardCompetenceFromCourse(int? id, int? course)
        {

            if (id == null || course == null)
            {
                return NotFound();
            }

            if (_context.hardCompetenciesList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.hardCompetenciesList'  is null.");
            }

            var hardCompetenciesList = await _context.hardCompetenciesList.FindAsync(id);
            if (hardCompetenciesList != null)
            {
                _context.hardCompetenciesList.Remove(hardCompetenciesList);
            }

            await _context.SaveChangesAsync();

            return RedirectToAction("CreateNewCourse", "Course", new { id = course });
        }

        // GET: SoftCompetenciesLists/Create
        public IActionResult AddSoftSkillToCourse(int? course)
        {
            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName");
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level");
            ViewBag.Course = course;
            return View();
        }

        // POST: SoftCompetenciesLists/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSoftSkillToCourse([Bind("SoftCompetenciesListId,EducationId,SoftSkillsId,CompetenceLevelId")] SoftCompetenciesList softCompetenciesList)
        {
            if (_context.softCompetenciesList.Where(p => p.SoftSkillsId == softCompetenciesList.SoftSkillsId && p.EducationId == softCompetenciesList.EducationId).Any())
            {
                ModelState.AddModelError("", "Личностное качество уже добавлено");
                ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName");
                ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level");
                ViewBag.Course = softCompetenciesList.EducationId;
                return View(softCompetenciesList);
            }

            if (ModelState.IsValid)
            {
                _context.Add(softCompetenciesList);
                await _context.SaveChangesAsync();
                return RedirectToAction("CreateNewCourse", "Course", new { id = softCompetenciesList.EducationId });
            }

            ViewData["SoftSkillsId"] = new SelectList(_context.softSkills, "SoftSkillsId", "CompetencyName");
            ViewData["CompetenceLevelId"] = new SelectList(_context.competenceLevel, "CompetenceLevelId", "Level");
            return View(softCompetenciesList);
        }

        public async Task<IActionResult> RemoveSoftSkillFromCourse(int? id, int? course)
        {

            if (id == null || course == null)
            {
                return NotFound();
            }

            if (_context.softCompetenciesList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.softCompetenciesList'  is null.");
            }

            var softCompetenciesList = await _context.softCompetenciesList.FindAsync(id);
            if (softCompetenciesList != null)
            {
                _context.softCompetenciesList.Remove(softCompetenciesList);
            }

            await _context.SaveChangesAsync();

            return RedirectToAction("CreateNewCourse", "Course", new { id = course });
        }


        [HttpPost]
        public async Task<ActionResult> ChangeName(Model model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var obj = new Education();

                    obj = await _context.education.FindAsync(model.id);
                    if (obj != null)
                    {
                        obj.CourseName = model.name;
                        _context.Update(obj);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        return Json(false);
                    }

                    return Json(true);
                }
                catch (Exception)
                {
                    return Json(false);
                }
            }
            else
            {
                return Json(false);
            }
        }

        public class Model
        {
            public int id { get; set; }
            public string name { get; set; }
        }


        // GET: EducationLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.educationList == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");

            if (!_context.educationList.Any(e => e.EducationListId == id && e.EmployeeId == user.Id))
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            var educationList = await _context.educationList.FindAsync(id);
            if (educationList == null)
            {
                return NotFound();
            }
            ViewData["EducationId"] = new SelectList(_context.education, "EducationId", "CourseName", educationList.EducationId);
            return View(educationList);
        }

        // POST: EducationLists/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EducationListId,EmployeeId,EducationId,PassageDateStart,PassageDateFinish,Certificate,Comment")] EducationList educationList)
        {
            if (id != educationList.EducationListId)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            educationList.EmployeeId = user.Id;
            if (!_context.educationList.Any(e => e.EducationListId == id && e.EmployeeId == user.Id))
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (educationList.PassageDateStart > educationList.PassageDateFinish)
                    {
                        ModelState.AddModelError("", "Проверьте даты начала и окончания курса");
                        ViewData["EducationId"] = new SelectList(_context.education, "EducationId", "CourseName", educationList.EducationId);
                        return View(educationList);
                    }
                    else
                    {
                        string ll = _context.education.FirstOrDefault(p => p.EducationId == educationList.EducationId).CourseName;
                        Logging logging = new Logging
                        {
                            Date = DateTime.Now,
                            EmployeeId = user.Id,
                            Description = "Изменен курс " + ll,
                        };
                        _context.Add(logging);

                        _context.Update(educationList);
                        await _context.SaveChangesAsync();
                        //return RedirectToAction("Courses");
                    }
                    /*_context.Update(educationList);
                    await _context.SaveChangesAsync();*/
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.educationList.Any(e => e.EducationListId == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Courses");
            }
            ViewData["EducationId"] = new SelectList(_context.education, "EducationId", "CourseName", educationList.EducationId);
            return View(educationList);
        }

        // GET: Educations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.educationList == null)
            {
                return NotFound();
            }

            if (_context.educationList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.education'  is null.");
            }

            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");

            if (!_context.educationList.Any(e => e.EducationListId == id && e.EmployeeId == user.Id))
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            var education = await _context.educationList
                .FirstOrDefaultAsync(m => m.EducationListId == id);
            if (education == null)
            {
                return NotFound();
            }

            education = await _context.educationList.FindAsync(id);

            if (education != null)
            {
                string ll = _context.education.FirstOrDefault(p => p.EducationId == education.EducationId).CourseName;
                Logging logging = new Logging
                {
                    Date = DateTime.Now,
                    EmployeeId = user.Id,
                    Description = "Удален курс " + ll,
                };
                _context.Add(logging);

                _context.educationList.Remove(education);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction("Courses");
        }
    }
}
