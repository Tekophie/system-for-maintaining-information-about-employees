﻿using InformationSystem.Data.Models.QuarterlyPlans;
using InformationSystem.Data.Models;
using InformationSystem.Data;
using InformationSystem.ViewModels.QuarterlyPlans;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Identity;
using InformationSystem.Data.Models.Project;
using InformationSystem.ViewModels.Skills;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNet.Identity;
using Microsoft.Build.ObjectModelRemoting;

namespace InformationSystem.Controllers.QuarterlyPlans
{
    public class QuarterlyDevelopmentPlanLists1Controller : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly Microsoft.AspNetCore.Identity.UserManager<User> _userManager;

        public QuarterlyDevelopmentPlanLists1Controller(ApplicationDbContext context, Microsoft.AspNetCore.Identity.UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: QuarterlyDevelopmentPlanLists1
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var applicationDbContext = await _context.quarterlyDevelopmentPlanList.Include(p => p.QuarterlyDevelopmentPlan)
                                                                                  .Where(p => p.EmployeeId == user.Id 
                                                                                  && p.QuarterlyDevelopmentPlan.Objective.Length > 0).ToListAsync();

            List<PlanView> plans = new List<PlanView>();
            foreach (var item in applicationDbContext)
            {
                plans.Add(new PlanView
                {
                    quarterlyDevelopmentPlanList = item,
                    planParts = _context.planPartsList.Where(p => p.PlanId == item.QuarterlyDevelopmentPlanId)
                    .Include(l => l.PlanParts)
                    .ToList()

                });
            }

            return View(plans);
        }

        // GET: QuarterlyDevelopmentPlanLists1/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList
                .FirstOrDefaultAsync(m => m.PlansId == id);
            if (quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            return View(quarterlyDevelopmentPlanList);
        }

        // GET: QuarterlyDevelopmentPlanLists1/Create
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUserAsync(User);

            ViewData["QuarterlyDevelopmentPlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId");
            ViewData["Employee"] = user.Id;
            ViewData["Objective"] = new SelectList(_context.quarterlyDevelopmentPlan.Where(p => p.Objective != null && p.UserId == user.Id), "PlanId", "Objective").Reverse();
            return View();
        }

        // POST: QuarterlyDevelopmentPlanLists1/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlansId,EmployeeId,QuarterlyDevelopmentPlanId")] QuarterlyDevelopmentPlanList quarterlyDevelopmentPlanList)
        {
            var user = await _userManager.GetUserAsync(User); if (user == null) return RedirectToAction("Login", "Account");


            if (ModelState.IsValid)
            {
                if (_context.quarterlyDevelopmentPlanList.Where(p=>p.EmployeeId == user.Id && p.QuarterlyDevelopmentPlanId == quarterlyDevelopmentPlanList.QuarterlyDevelopmentPlanId).Any())
                {
                    ModelState.AddModelError("", "Выбранный план уже добавлен");
                }
                else 
                {
                    /*var ll = _context.quarterlyDevelopmentPlan.FirstOrDefault(p => p.PlanId == quarterlyDevelopmentPlanList.QuarterlyDevelopmentPlanId);
                    quarterlyDevelopmentPlanList.EmployeeId = user.Id;

                    Logging logging = new Logging
                    {
                        Date = DateTime.Now,
                        EmployeeId = user.Id,
                        Description = "Добавлен план квартального развития " + ll.Objective,
                    };

                    _context.Add(logging);*/
                    _context.Add(quarterlyDevelopmentPlanList);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                
            }
            ViewData["QuarterlyDevelopmentPlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId");
            ViewData["Employee"] = user.Id;
            ViewData["Objective"] = new SelectList(_context.quarterlyDevelopmentPlan.Where(p => p.Objective != null && p.UserId == user.Id), "PlanId", "Objective", quarterlyDevelopmentPlanList.PlansId).Reverse();
            return View(quarterlyDevelopmentPlanList);
        }

        // GET: QuarterlyDevelopmentPlans/Create
        public async Task<IActionResult> CreateNewQuarterlyPlan(int? id, bool edit)
        {
            var user = await _userManager.GetUserAsync(User); if (user == null) return RedirectToAction("Login", "Account");

            var quarterlyDevelopmentPlan = new QuarterlyDevelopmentPlan();
            if (id == null || _context.quarterlyDevelopmentPlan == null)
            {
                quarterlyDevelopmentPlan.Quarter = string.Empty;
                quarterlyDevelopmentPlan.Objective = string.Empty;
                quarterlyDevelopmentPlan.DiscussDate = DateTime.Now;
                quarterlyDevelopmentPlan.UserId = user.Id;

                if (ModelState.IsValid)
                {                    
                    _context.Add(quarterlyDevelopmentPlan);
                    await _context.SaveChangesAsync();
                    id = quarterlyDevelopmentPlan.PlanId;

                    var quarterlyDevelopmentPlanList = new QuarterlyDevelopmentPlanList();
                    quarterlyDevelopmentPlanList.QuarterlyDevelopmentPlanId = quarterlyDevelopmentPlan.PlanId;
                    quarterlyDevelopmentPlanList.EmployeeId = user.Id;

                    _context.Add(quarterlyDevelopmentPlanList);
                    await _context.SaveChangesAsync();

                    //return RedirectToAction("AddCourse");
                    return RedirectToAction("CreateNewQuarterlyPlan", "QuarterlyDevelopmentPlanLists1", new { id = id });
                }
            }
            else
            {
                quarterlyDevelopmentPlan = await _context.quarterlyDevelopmentPlan.FindAsync(id);
                if (quarterlyDevelopmentPlan == null)
                {
                    return NotFound();
                }
            }

            ViewBag.Edit = false;
            if (edit == true)
            {
                ViewBag.Edit = edit;
            }

            var obj = new PlansViewModel
            {
                quarterlyDevelopmentPlan = quarterlyDevelopmentPlan,
                planPartsList = await _context.planPartsList.Include(p => p.PlanParts)
                                            .Where(p => p.PlanId == id)
                                            .ToListAsync(),
            };
            return View(obj);
        }

        // POST: QuarterlyDevelopmentPlans/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateNewQuarterlyPlan(int id, [Bind("PlanId,Quarter,Objective,AchievementCriteria,BenefitsForEmployee,BenefitsForCompany,Discuss,DiscussDate,UserId")] QuarterlyDevelopmentPlan quarterlyDevelopmentPlan, bool parameterEdit)
        {
            var user = await _userManager.GetUserAsync(User); if (user == null) return RedirectToAction("Login", "Account");
            quarterlyDevelopmentPlan.UserId = user.Id;

            if (id != quarterlyDevelopmentPlan.PlanId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //добавление лога
                    if (parameterEdit)
                    {
                        string ll = quarterlyDevelopmentPlan.PlanId + " " + quarterlyDevelopmentPlan.Objective;
                        Logging logging = new Logging
                        {
                            Date = DateTime.Now,
                            EmployeeId = user.Id,
                            Description = "Изменен план " + ll,
                        };
                        _context.Add(logging);
                    }
                    else
                    {
                        string ll = quarterlyDevelopmentPlan.PlanId + " " + quarterlyDevelopmentPlan.Objective;
                        Logging logging = new Logging
                        {
                            Date = DateTime.Now,
                            EmployeeId = user.Id,
                            Description = "Добавлен план " + ll,
                        };
                        _context.Add(logging);
                    }

                    _context.Update(quarterlyDevelopmentPlan);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuarterlyDevelopmentPlanExists(quarterlyDevelopmentPlan.PlanId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", "QuarterlyDevelopmentPlanLists1");
            }

            return View(quarterlyDevelopmentPlan);

        }

        private bool QuarterlyDevelopmentPlanExists(int id)
        {
            return _context.projects.Any(e => e.Id == id);
        }

        // GET: PlanPartsLists/Create
        public async Task<IActionResult> AddPlanPart(int? quarterlyPlan)
        {
            var user = await _userManager.GetUserAsync(User);
            ViewData["PlanPartsId"] = new SelectList(_context.planParts, "PlanPartsId", "MeasurableResult").Reverse();
            ViewData["PlanPartsId"] = new SelectList(_context.planParts.Where(p => p.UserId == user.Id), "PlanPartsId", "MeasurableResult").Reverse();
            
            ViewData["PlanId"] = quarterlyPlan;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPlanPart([Bind("PlanPartsListId,PlanPartsId,PlanId")] PlanPartsList planPartsList)
        {
            var user = await _userManager.GetUserAsync(User);
            //planPartsList.EmployeeId = user.Id;

            if (ModelState.IsValid)
            {
                if (_context.planPartsList.Where(p =>   p.PlanId == planPartsList.PlanId
                                                        && p.PlanPartsId == planPartsList.PlanPartsId).Any())
                {
                    ModelState.AddModelError("", "Выбранный план уже добавлен");
                }
                else
                {
                    _context.Add(planPartsList);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("CreateNewQuarterlyPlan", "QuarterlyDevelopmentPlanLists1", new { id = planPartsList.PlanId });
                }
            }

            ViewData["PlanPartsId"] = new SelectList(_context.planParts, "PlanPartsId", "MeasurableResult").Reverse();
            ViewData["PlanId"] = planPartsList.PlanId;

            return View();
        }

        // POST: PlanPartsLists/Delete/5
        public async Task<IActionResult> RemovePlanPart(int id, int? planPart)
        {
            var user = await _userManager.GetUserAsync(User);
            if (_context.planPartsList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.planPartsList'  is null.");
            }

            var planPartsList = await _context.planPartsList.FindAsync(id);
            if (planPartsList != null)
            {
                var ll = _context.quarterlyDevelopmentPlan.FirstOrDefault(p => p.PlanId == planPartsList.PlanId);
                Logging logging = new Logging
                {
                    Date = DateTime.Now,
                    EmployeeId = user.Id,
                    Description = "Удален план квартального развития " + ll.Objective,
                };
                _context.Add(logging);
                _context.planPartsList.Remove(planPartsList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction("CreateNewQuarterlyPlan", "QuarterlyDevelopmentPlanLists1", new { id = planPartsList.PlanId });

        }

        public async Task<IActionResult> CreateNewQuarterlyPlan2()
        {
            return RedirectToAction("Create", "QuarterlyDevelopmentPlans");
        }

        // GET: QuarterlyDevelopmentPlanLists1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList.FindAsync(id);
            if (quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }
            ViewData["PlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId", quarterlyDevelopmentPlanList.QuarterlyDevelopmentPlanId);
            return View(quarterlyDevelopmentPlanList);
        }

        public async Task<IActionResult> CreateNewPlan(int? plan)
        {
            if (plan == null)
            {
                return NotFound();
            }

            ViewBag.Plan = plan;

            var user = await _userManager.GetUserAsync(User);
            ViewBag.UserId = user.Id;
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateNewPlan(int plan, [Bind("PlanPartsId,ActivityDirection,MeasurableResult,Deadline,RatingInHours,CurrentProgress,UserId")] PlanParts planParts)
        {
            var user = await _userManager.GetUserAsync(User);
            planParts.UserId = user.Id;

            //ModelState["UserId"].Errors.Clear();            
            //await TryUpdateModelAsync(planParts);
            ModelState.Clear();

            if (ModelState.IsValid)
            {
                _context.Add(planParts);
                await _context.SaveChangesAsync();
                return RedirectToAction("AddPlanPart", new { quarterlyPlan = plan });
            }
            ViewBag.Plan = plan;
            return PartialView(planParts);
        }

        // POST: QuarterlyDevelopmentPlanLists1/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlansId,EmployeeId,QuarterlyDevelopmentPlanId")] QuarterlyDevelopmentPlanList quarterlyDevelopmentPlanList)
        {
            if (id != quarterlyDevelopmentPlanList.PlansId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(quarterlyDevelopmentPlanList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuarterlyDevelopmentPlanListExists(quarterlyDevelopmentPlanList.PlansId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId", quarterlyDevelopmentPlanList.QuarterlyDevelopmentPlanId);
            return View(quarterlyDevelopmentPlanList);
        }

        // GET: QuarterlyDevelopmentPlanLists1/Delete/5
        public async Task<IActionResult> Delete2(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList
                .FirstOrDefaultAsync(m => m.PlansId == id);
            if (quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            return View(quarterlyDevelopmentPlanList);
        }

        // POST: QuarterlyDevelopmentPlanLists1/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int? id)
        {
            if (_context.quarterlyDevelopmentPlanList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.quarterlyDevelopmentPlanList'  is null.");
            }
            var user = await _userManager.GetUserAsync(User); if (user == null) return RedirectToAction("Login", "Account");            

            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList.FindAsync(id);

            if (!_context.quarterlyDevelopmentPlanList.Where(p => p.PlansId == id && p.EmployeeId==user.Id).Any())
            {
                return RedirectToAction("AccessDenied", "Home");
            }

            if (quarterlyDevelopmentPlanList != null)
            {
                var ll = _context.quarterlyDevelopmentPlan.FirstOrDefault(p => p.PlanId == quarterlyDevelopmentPlanList.QuarterlyDevelopmentPlanId);
                Logging logging = new Logging
                {
                    Date = DateTime.Now,
                    EmployeeId = user.Id,
                    Description = "Удален план квартального развития " + ll.Objective,
                };
                _context.Add(logging);

                _context.quarterlyDevelopmentPlanList.Remove(quarterlyDevelopmentPlanList);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QuarterlyDevelopmentPlanListExists(int id)
        {
          return _context.quarterlyDevelopmentPlanList.Any(e => e.PlansId == id);
        }

        [HttpPost]
        public async Task<ActionResult> UpdatePlane(QuarterlyDevelopmentPlan model)
        {
            var user = await _userManager.GetUserAsync(User); if (user == null) return RedirectToAction("Login", "Account");
            model.UserId = user.Id;

            if (ModelState.IsValid)
            {
                try
                {
                    
                    var obj = new QuarterlyDevelopmentPlan();

                    obj = await _context.quarterlyDevelopmentPlan.FindAsync(model.PlanId);
                    if (obj != null)
                    {
                        if (model.Quarter != null)
                        {
                            obj.Quarter = model.Quarter;
                        }
                        if (model.Objective != null)
                        {
                            obj.Objective = model.Objective;
                        }
                        if (model.AchievementCriteria != null)
                        {
                            obj.AchievementCriteria = model.AchievementCriteria;
                        }
                        if (model.BenefitsForEmployee != null)
                        {
                            obj.BenefitsForEmployee = model.BenefitsForEmployee;
                        }
                        if (model.BenefitsForCompany != null)
                        {
                            obj.BenefitsForCompany = model.BenefitsForCompany;
                        }
                        if (model.Discuss != null)
                        {
                            obj.Discuss = model.Discuss;
                        }
                        if (model.DiscussDate != null)
                        {
                            obj.DiscussDate = model.DiscussDate;
                        }

                        _context.Update(obj);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        return Json(false);
                    }

                    return Json(true);
                }
                catch (Exception)
                {
                    return Json(false);
                }
            }
            else
            {
                return Json(false);
            }
        }
    }

    public class PlanView
    {
        public QuarterlyDevelopmentPlanList? quarterlyDevelopmentPlanList { get; set; }

        public List<PlanPartsList>? planParts { get; set; }
    }
}
