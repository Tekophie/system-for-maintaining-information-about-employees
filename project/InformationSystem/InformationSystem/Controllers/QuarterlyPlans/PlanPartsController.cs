﻿using InformationSystem.Data;
using InformationSystem.Data.Models;
using InformationSystem.Data.Models.QuarterlyPlans;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.QuarterlyPlans
{
    public class PlanPartsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlanPartsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PlanParts
        public async Task<IActionResult> Index()
        {
            return View(await _context.planParts.ToListAsync());
        }

        // GET: PlanParts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.planParts == null)
            {
                return NotFound();
            }

            var planParts = await _context.planParts
                .FirstOrDefaultAsync(m => m.PlanPartsId == id);
            if (planParts == null)
            {
                return NotFound();
            }

            return View(planParts);
        }

        // GET: PlanParts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlanParts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlanPartsId,ActivityDirection,MeasurableResult,Deadline,RatingInHours,CurrentProgress")] PlanParts planParts)
        {
            if (ModelState.IsValid)
            {
                _context.Add(planParts);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(planParts);
        }

        // GET: PlanParts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.planParts == null)
            {
                return NotFound();
            }

            var planParts = await _context.planParts.FindAsync(id);
            if (planParts == null)
            {
                return NotFound();
            }
            return View(planParts);
        }

        // POST: PlanParts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlanPartsId,ActivityDirection,MeasurableResult,Deadline,RatingInHours,CurrentProgress")] PlanParts planParts)
        {
            if (id != planParts.PlanPartsId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(planParts);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlanPartsExists(planParts.PlanPartsId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(planParts);
        }

        // GET: PlanParts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.planParts == null)
            {
                return NotFound();
            }

            var planParts = await _context.planParts
                .FirstOrDefaultAsync(m => m.PlanPartsId == id);
            if (planParts == null)
            {
                return NotFound();
            }

            return View(planParts);
        }

        // POST: PlanParts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.planParts == null)
            {
                return Problem("Entity set 'ApplicationDbContext.planParts'  is null.");
            }
            var planParts = await _context.planParts.FindAsync(id);
            if (planParts != null)
            {
                _context.planParts.Remove(planParts);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlanPartsExists(int id)
        {
            return _context.planParts.Any(e => e.PlanPartsId == id);
        }
    }
}
