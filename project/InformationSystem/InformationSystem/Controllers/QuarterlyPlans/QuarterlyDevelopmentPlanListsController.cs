﻿using InformationSystem.Data;
using InformationSystem.Data.Models.QuarterlyPlans;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.QuarterlyPlans
{
    public class QuarterlyDevelopmentPlanListsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public QuarterlyDevelopmentPlanListsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: QuarterlyDevelopmentPlanLists
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.quarterlyDevelopmentPlanList.Include(q => q.QuarterlyDevelopmentPlan);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: QuarterlyDevelopmentPlanLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList
                .Include(q => q.QuarterlyDevelopmentPlan)
                .FirstOrDefaultAsync(m => m.EmployeeId == id);
            if (quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            return View(quarterlyDevelopmentPlanList);
        }

        // GET: QuarterlyDevelopmentPlanLists/Create
        public IActionResult Create()
        {
            ViewData["PlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId");
            ViewData["Employee"] = new SelectList(_context.Users, "Id", "Name");
            ViewData["Objective"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "Objective");
            return View();
        }

        // POST: QuarterlyDevelopmentPlanLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlansId,EmployeeId,PlanId")] QuarterlyDevelopmentPlanList quarterlyDevelopmentPlanList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(quarterlyDevelopmentPlanList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId", quarterlyDevelopmentPlanList.PlanId);
            return View(quarterlyDevelopmentPlanList);
        }

        // GET: QuarterlyDevelopmentPlanLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList.FindAsync(id);
            if (quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }
            ViewData["PlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId", quarterlyDevelopmentPlanList.PlanId);
            return View(quarterlyDevelopmentPlanList);
        }

        // POST: QuarterlyDevelopmentPlanLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlansId,EmployeeId,PlanId")] QuarterlyDevelopmentPlanList quarterlyDevelopmentPlanList)
        {
            if (id != quarterlyDevelopmentPlanList.EmployeeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(quarterlyDevelopmentPlanList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuarterlyDevelopmentPlanListExists(quarterlyDevelopmentPlanList.EmployeeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId", quarterlyDevelopmentPlanList.PlanId);
            return View(quarterlyDevelopmentPlanList);
        }

        // GET: QuarterlyDevelopmentPlanLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList
                .Include(q => q.QuarterlyDevelopmentPlan)
                .FirstOrDefaultAsync(m => m.EmployeeId == id);
            if (quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            return View(quarterlyDevelopmentPlanList);
        }

        // POST: QuarterlyDevelopmentPlanLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.quarterlyDevelopmentPlanList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.quarterlyDevelopmentPlanList'  is null.");
            }
            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList.FindAsync(id);
            if (quarterlyDevelopmentPlanList != null)
            {
                _context.quarterlyDevelopmentPlanList.Remove(quarterlyDevelopmentPlanList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QuarterlyDevelopmentPlanListExists(int id)
        {
            return _context.quarterlyDevelopmentPlanList.Any(e => e.EmployeeId == id);
        }
    }
}
