﻿using InformationSystem.Data;
using InformationSystem.Data.Models.QuarterlyPlans;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.QuarterlyPlans
{
    public class PlanPartsListsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlanPartsListsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PlanPartsLists
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.planPartsList.Include(p => p.PlanParts).Include(p => p.QuarterlyDevelopmentPlan);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: PlanPartsLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.planPartsList == null)
            {
                return NotFound();
            }

            var planPartsList = await _context.planPartsList
                .Include(p => p.PlanParts)
                .Include(p => p.QuarterlyDevelopmentPlan)
                .FirstOrDefaultAsync(m => m.PlanPartsId == id);
            if (planPartsList == null)
            {
                return NotFound();
            }

            return View(planPartsList);
        }

        // GET: PlanPartsLists/Create
        public IActionResult Create()
        {
            ViewData["ActivityDirection"] = new SelectList(_context.planParts, "PlanPartsId", "ActivityDirection");
            ViewData["Objective"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "Objective");
            return View();
        }

        // POST: PlanPartsLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlanPartsListId,PlanPartsId,PlanId")] PlanPartsList planPartsList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(planPartsList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PlanPartsId"] = new SelectList(_context.planParts, "PlanPartsId", "PlanPartsId", planPartsList.PlanPartsId);
            ViewData["PlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId", planPartsList.PlanId);
            return View(planPartsList);
        }

        // GET: PlanPartsLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.planPartsList == null)
            {
                return NotFound();
            }

            var planPartsList = await _context.planPartsList.FindAsync(id);
            if (planPartsList == null)
            {
                return NotFound();
            }
            ViewData["PlanPartsId"] = new SelectList(_context.planParts, "PlanPartsId", "PlanPartsId", planPartsList.PlanPartsId);
            ViewData["PlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId", planPartsList.PlanId);
            return View(planPartsList);
        }

        // POST: PlanPartsLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlanPartsListId,PlanPartsId,PlanId")] PlanPartsList planPartsList)
        {
            if (id != planPartsList.PlanPartsId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(planPartsList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlanPartsListExists(planPartsList.PlanPartsId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PlanPartsId"] = new SelectList(_context.planParts, "PlanPartsId", "PlanPartsId", planPartsList.PlanPartsId);
            ViewData["PlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId", planPartsList.PlanId);
            return View(planPartsList);
        }

        // GET: PlanPartsLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.planPartsList == null)
            {
                return NotFound();
            }

            var planPartsList = await _context.planPartsList
                .Include(p => p.PlanParts)
                .Include(p => p.QuarterlyDevelopmentPlan)
                .FirstOrDefaultAsync(m => m.PlanPartsId == id);
            if (planPartsList == null)
            {
                return NotFound();
            }

            return View(planPartsList);
        }

        // POST: PlanPartsLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.planPartsList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.planPartsList'  is null.");
            }
            var planPartsList = await _context.planPartsList.FindAsync(id);
            if (planPartsList != null)
            {
                _context.planPartsList.Remove(planPartsList);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlanPartsListExists(int id)
        {
            return _context.planPartsList.Any(e => e.PlanPartsId == id);
        }
    }
}
