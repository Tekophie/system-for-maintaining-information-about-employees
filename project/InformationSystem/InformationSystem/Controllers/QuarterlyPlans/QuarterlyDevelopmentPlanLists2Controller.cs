﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using InformationSystem.Data;
using InformationSystem.Data.Models.QuarterlyPlans;
using InformationSystem.Data.Models;
using Microsoft.AspNetCore.Identity;

namespace InformationSystem.Controllers.QuarterlyPlans
{
    public class QuarterlyDevelopmentPlanLists2Controller : Controller
    {
        private readonly ApplicationDbContext _context;
        readonly UserManager<User> _userManager;

        public QuarterlyDevelopmentPlanLists2Controller(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: QuarterlyDevelopmentPlanLists2
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var applicationDbContext = _context.quarterlyDevelopmentPlanList.Include(q => q.QuarterlyDevelopmentPlan);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: QuarterlyDevelopmentPlanLists2/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList
                .Include(q => q.QuarterlyDevelopmentPlan)
                .FirstOrDefaultAsync(m => m.PlansId == id);
            if (quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            return View(quarterlyDevelopmentPlanList);
        }

        // GET: QuarterlyDevelopmentPlanLists2/Create
        public IActionResult Create()
        {
            ViewData["QuarterlyDevelopmentPlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId");
            ViewData["Employee"] = new SelectList(_context.Users, "Id", "Name");
            ViewData["Objective"] = new SelectList(_context.quarterlyDevelopmentPlan, "QuarterlyDevelopmentPlanId", "Objective");
            return View();
        }

        // POST: QuarterlyDevelopmentPlanLists2/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlansId,EmployeeId,QuarterlyDevelopmentPlanId")] QuarterlyDevelopmentPlanList quarterlyDevelopmentPlanList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(quarterlyDevelopmentPlanList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["QuarterlyDevelopmentPlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId", quarterlyDevelopmentPlanList.QuarterlyDevelopmentPlanId);
            return View(quarterlyDevelopmentPlanList);
        }

        // GET: QuarterlyDevelopmentPlanLists2/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList.FindAsync(id);
            if (quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }
            ViewData["QuarterlyDevelopmentPlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId", quarterlyDevelopmentPlanList.QuarterlyDevelopmentPlanId);
            return View(quarterlyDevelopmentPlanList);
        }

        // POST: QuarterlyDevelopmentPlanLists2/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlansId,EmployeeId,QuarterlyDevelopmentPlanId")] QuarterlyDevelopmentPlanList quarterlyDevelopmentPlanList)
        {
            if (id != quarterlyDevelopmentPlanList.PlansId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(quarterlyDevelopmentPlanList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuarterlyDevelopmentPlanListExists(quarterlyDevelopmentPlanList.PlansId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["QuarterlyDevelopmentPlanId"] = new SelectList(_context.quarterlyDevelopmentPlan, "PlanId", "PlanId", quarterlyDevelopmentPlanList.QuarterlyDevelopmentPlanId);
            return View(quarterlyDevelopmentPlanList);
        }

        // GET: QuarterlyDevelopmentPlanLists2/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList
                .Include(q => q.QuarterlyDevelopmentPlan)
                .FirstOrDefaultAsync(m => m.PlansId == id);
            if (quarterlyDevelopmentPlanList == null)
            {
                return NotFound();
            }

            return View(quarterlyDevelopmentPlanList);
        }

        // POST: QuarterlyDevelopmentPlanLists2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.quarterlyDevelopmentPlanList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.quarterlyDevelopmentPlanList'  is null.");
            }
            var quarterlyDevelopmentPlanList = await _context.quarterlyDevelopmentPlanList.FindAsync(id);
            if (quarterlyDevelopmentPlanList != null)
            {
                _context.quarterlyDevelopmentPlanList.Remove(quarterlyDevelopmentPlanList);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QuarterlyDevelopmentPlanListExists(int id)
        {
          return _context.quarterlyDevelopmentPlanList.Any(e => e.PlansId == id);
        }
    }
}
