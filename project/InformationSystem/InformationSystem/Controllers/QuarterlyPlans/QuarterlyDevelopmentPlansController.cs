﻿using InformationSystem.Data;
using InformationSystem.Data.Models.QuarterlyPlans;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers.QuarterlyPlans
{
    public class QuarterlyDevelopmentPlansController : Controller
    {
        private readonly ApplicationDbContext _context;

        public QuarterlyDevelopmentPlansController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: QuarterlyDevelopmentPlans
        public async Task<IActionResult> Index()
        {
            return View(await _context.quarterlyDevelopmentPlan.ToListAsync());
        }

        // GET: QuarterlyDevelopmentPlans/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlan == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlan = await _context.quarterlyDevelopmentPlan
                .FirstOrDefaultAsync(m => m.PlanId == id);
            if (quarterlyDevelopmentPlan == null)
            {
                return NotFound();
            }

            return View(quarterlyDevelopmentPlan);
        }

        // GET: QuarterlyDevelopmentPlans/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: QuarterlyDevelopmentPlans/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlanId,Quarter,Objective,AchievementCriteria,BenefitsForEmployee,BenefitsForCompany,Discuss,DiscussDate")] QuarterlyDevelopmentPlan quarterlyDevelopmentPlan)
        {
            if (ModelState.IsValid)
            {
                _context.Add(quarterlyDevelopmentPlan);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(quarterlyDevelopmentPlan);
        }

        // GET: QuarterlyDevelopmentPlans/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlan == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlan = await _context.quarterlyDevelopmentPlan.FindAsync(id);
            if (quarterlyDevelopmentPlan == null)
            {
                return NotFound();
            }
            return View(quarterlyDevelopmentPlan);
        }

        // POST: QuarterlyDevelopmentPlans/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlanId,Quarter,Objective,AchievementCriteria,BenefitsForEmployee,BenefitsForCompany,Discuss,DiscussDate")] QuarterlyDevelopmentPlan quarterlyDevelopmentPlan)
        {
            if (id != quarterlyDevelopmentPlan.PlanId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(quarterlyDevelopmentPlan);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuarterlyDevelopmentPlanExists(quarterlyDevelopmentPlan.PlanId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(quarterlyDevelopmentPlan);
        }

        // GET: QuarterlyDevelopmentPlans/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.quarterlyDevelopmentPlan == null)
            {
                return NotFound();
            }

            var quarterlyDevelopmentPlan = await _context.quarterlyDevelopmentPlan
                .FirstOrDefaultAsync(m => m.PlanId == id);
            if (quarterlyDevelopmentPlan == null)
            {
                return NotFound();
            }

            return View(quarterlyDevelopmentPlan);
        }

        // POST: QuarterlyDevelopmentPlans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.quarterlyDevelopmentPlan == null)
            {
                return Problem("Entity set 'ApplicationDbContext.quarterlyDevelopmentPlan'  is null.");
            }
            var quarterlyDevelopmentPlan = await _context.quarterlyDevelopmentPlan.FindAsync(id);
            if (quarterlyDevelopmentPlan != null)
            {
                _context.quarterlyDevelopmentPlan.Remove(quarterlyDevelopmentPlan);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QuarterlyDevelopmentPlanExists(int id)
        {
            return _context.quarterlyDevelopmentPlan.Any(e => e.PlanId == id);
        }
    }
}
