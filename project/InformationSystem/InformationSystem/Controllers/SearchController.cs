﻿using InformationSystem.Data.Models;
using InformationSystem.ViewModels.Mentors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using InformationSystem.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using InformationSystem.Data.Models.SkillsAndEducation;
using System.Linq;
using InformationSystem.Data.Models.Project;
using NuGet.Packaging.Signing;
using NuGet.Packaging;
using InformationSystem.Data.TagHelpers;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Controllers
{
    public class SearchController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ApplicationDbContext _context;

        public SearchController(ApplicationDbContext context, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;

        }

        public async Task<IActionResult> IndexAsync(string? SearchEmail, 
                                   string? SearchFamily, 
                                   string? SearchName,
                                   int[]? SearchCourseId,
                                   int[]? SearchProjectId,
                                   List<int?> SearchSoftId,
                                   List<int?> SearchHardId,
                                   List<int?> SearchSoftLevel,
                                   List<int?> SearchHardLevel,
                                   int page = 1)
        {

            var user = await _userManager.GetUserAsync(User); if (user == null) return RedirectToAction("Login", "Account");
            var userId = user.Id;

            List<User> allUsers = _userManager.Users.Where(p => p.isActive != false).ToList();
            allUsers.Remove(user);

            //фильтры
            try
            {
                if (SearchEmail != null)
                {
                    allUsers = (List<User>)allUsers.Where(x => x.Email.Contains(SearchEmail)).ToList();
                    ViewBag.SearchEmail = SearchEmail;
                }

                if (SearchFamily != null)
                {
                    allUsers = (List<User>)allUsers.Where(x => x.Surname.Contains(SearchFamily)).ToList();
                    ViewBag.SearchFamily = SearchFamily;
                }

                if (SearchName != null)
                {
                    allUsers = (List<User>)allUsers.Where(x => x.Name.Contains(SearchName)).ToList();
                    ViewBag.SearchName = SearchName;
                }

                if (SearchCourseId.Length > 0)
                {
                    List<EducationList> getUsersCourse = _context.educationList.Where(p => SearchCourseId.Any(g => p.EducationId == g)).ToList();

                    allUsers = (List<User>)allUsers.Where(p => getUsersCourse.Any(g => p.Id == g.EmployeeId)).ToList();

                }


                if (SearchProjectId.Length > 0)
                {
                    //List<ProjectList> getUsersProject = _context.projectList.Where(p => SearchProjectId.Any(g => p.ProjectsId == g)).ToList();
                    
                    List<ProjectList> getUsersProject = _context.projectList.Where(p => SearchProjectId.Any(g => (int)p.ProjectsId == g)).ToList();

                    allUsers = (List<User>)allUsers.Where(p => getUsersProject.Any(g => p.Id == g.EmployeeId)).ToList();
                }

                if (SearchSoftId.FindAll(c => c != null).Count > 0)
                {
                    List<SoftSkillsList> getUsersSoft = new List<SoftSkillsList>();
                    List<SoftSkillsList> getUsersSoftTemp = null;

                    //getUsersSoft = _context.softSkillsList.Where(p => SearchSoftId.(g => p.SoftSkillsId == g) && SearchSoftLevel.Any(h => p.CompetenceLevelId == h)).ToList();
                    var SoftSkillsAndLevels = SearchSoftId.Zip(SearchSoftLevel, (s, l) => new { Skill = s, Level = l }).ToList();
                    int i = 0;
                   // getUsersSoft = _context.softSkillsList.Where(p => p.SoftSkillsId == ).ToList();
                    foreach (var softListItem in SoftSkillsAndLevels)
                        if (softListItem.Skill != null)
                            if (softListItem.Level != null)
                            { 
                                getUsersSoftTemp = _context.softSkillsList.Where(p => p.SoftSkillsId == softListItem.Skill && p.CompetenceLevelId == softListItem.Level).ToList();                                
                                if (getUsersSoftTemp.Count == 0)
                                    i++;
                                else
                                    getUsersSoft.AddRange(getUsersSoftTemp);
                            }                               
                            else
                            {
                                getUsersSoftTemp = _context.softSkillsList.Where(p => p.SoftSkillsId == softListItem.Skill).ToList();                                
                                if (getUsersSoftTemp.Count == 0)
                                    i++;
                                else
                                    getUsersSoft.AddRange(getUsersSoftTemp);
                            }

                    if(i > 0)
                        getUsersSoft = new List<SoftSkillsList>();
                    allUsers = (List<User>)allUsers.Where(p => getUsersSoft.Any(g => p.Id == g.EmployeeId)).ToList();
                }

                if (SearchHardId.FindAll(c => c != null).Count > 0)
                {
                    List<HardSkillsList> getUsersHard = new List<HardSkillsList>(); ;
                    List<HardSkillsList> getUsersHardTemp = null;

                    var HardSkillsAndLevels = SearchHardId.Zip(SearchHardLevel, (s, l) => new { Skill = s, Level = l });
                    int i = 0;
                    foreach (var hardListItem in HardSkillsAndLevels)
                        if (hardListItem.Skill != null)
                            if (hardListItem.Level != null)
                            {
                                getUsersHardTemp = _context.hardSkillsList.Where(p => p.HardSkillsId == hardListItem.Skill && p.CompetenceLevelId == hardListItem.Level).ToList();
                                if (getUsersHardTemp.Count == 0)
                                    i++;
                                else
                                    getUsersHard.AddRange(getUsersHardTemp);
                            }
                            else
                            {
                                getUsersHardTemp = _context.hardSkillsList.Where(p => p.HardSkillsId == hardListItem.Skill).ToList();
                                if (getUsersHardTemp.Count == 0)
                                    i++;
                                else
                                    getUsersHard.AddRange(getUsersHardTemp);
                            }

                    if (i > 0)
                        getUsersHard = new List<HardSkillsList>();
                    allUsers = (List<User>)allUsers.Where(p => getUsersHard.Any(g => p.Id == g.EmployeeId)).ToList();
                }   
            }
            catch (Exception)
            {
                ViewBag.SearchEmail = null;
                ViewBag.SearchFamily = null;
                ViewBag.SearchName = null;
            }

            bool isAdmin = User.IsInRole("Admin");
            bool isManager = User.IsInRole("Manager");

            //добавление кнопок пользователям
            List<AllUsersViewModel> allUsersView = new List<AllUsersViewModel>();
            foreach (var item in allUsers)
            {
                AllUsersViewModel buf = new AllUsersViewModel();
                buf.Surname = item.Surname;
                buf.Name = item.Name;
                buf.Email = item.Email;

                if (_context.mentors.Where(p => p.TraineeId == userId && p.TraineeConfirmed == true && p.MentorId == item.Id).Count() > 0)
                {
                    buf.showMentorButton = false;
                }
                else { buf.showMentorButton = true; }

                if (_context.mentors.Where(p => p.MentorId == userId && p.MentorConfirmed == true && p.TraineeId == item.Id).Count() > 0)
                {
                    buf.showTraineeButton = false;
                }
                else { 
                    buf.showTraineeButton = true; 
                }

                buf.showProfileButton = false;
                if (isAdmin || isManager)
                {
                    buf.showProfileButton = true;
                }
                //проверка наставника
                else if (_context.mentors.Where(p => p.MentorId == user.Id && p.TraineeId == userId
                                                        && p.TraineeConfirmed == true && p.MentorConfirmed == true).Any() == true)
                {
                    buf.showProfileButton = true;
                }


                allUsersView.Add(buf);
            }

            //передача данных для фильтров
            ViewBag.Courses = new MultiSelectList(_context.education.Where(p => p.CourseName != string.Empty), "EducationId", "CourseName", SearchCourseId.ToList());
            ViewBag.Projects = new MultiSelectList(_context.projects.Where(p => p.Name != string.Empty), "Id", "Name", SearchProjectId.ToList());
            ViewBag.SoftSkills = new SelectList(_context.softSkills.Where(p => p.CompetencyName != string.Empty), "SoftSkillsId", "CompetencyName");
            ViewBag.SoftLevels = new SelectList(_context.competenceLevel.Where(p => p.Level != string.Empty), "CompetenceLevelId", "Level");
            ViewBag.HardSkills = new SelectList(_context.hardSkills.Where(p => p.CompetencyName != string.Empty), "HardSkillsId", "CompetencyName");
            ViewBag.HardLevels = new SelectList(_context.competenceLevel.Where(p => p.Level != string.Empty), "CompetenceLevelId", "Level");

            ViewBag.AllUsers = allUsersView;


            int pageSize = 15;
            IQueryable<AllUsersViewModel> source = allUsersView.AsQueryable();
            var count = source.Count();
            var items = source.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            SearchViewModel viewModel = new SearchViewModel
            {
                PageViewModel = pageViewModel,
                AllUsers = items
            };

            return View(viewModel);
        }

        public async Task<IActionResult> AddSoftToList(int? SearchSoftId, int? SearchSoftLevel)
        {
            Dictionary<int, int> softSkills = new Dictionary<int, int>();
            softSkills = (Dictionary<int, int>)ViewData["SoftList"];

            if (SearchSoftId != null && SearchSoftLevel != null)
                softSkills.Add((int)SearchSoftId, (int)SearchSoftLevel);
            ViewData["SoftList"] = softSkills;
            return View();
        }

        public class SearchViewModel
        {
            public IEnumerable<AllUsersViewModel> AllUsers { get; set; }
            public PageViewModel PageViewModel { get; set; }
        }
    }
}
