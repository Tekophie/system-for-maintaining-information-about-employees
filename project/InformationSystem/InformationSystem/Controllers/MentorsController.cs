﻿using InformationSystem.Data;
using InformationSystem.Data.Models;
using InformationSystem.ViewModels.Mentors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace InformationSystem.Controllers
{
    public class MentorsController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ApplicationDbContext _context;
        private readonly IMentors _mentors;
        IConfiguration _configuration;

        public MentorsController(ApplicationDbContext context, UserManager<User> userManager, SignInManager<User> signInManager, IConfiguration configuration, IMentors mentors)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _mentors = mentors;
        }

        public async Task<ActionResult> MyMentors()
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            var userId = user.Id;

            ViewBag.notConfirmedFromTrainee = _mentors.notConfirmedFromTrainee(userId, false);
            ViewBag.notConfirmedFromMentor = _mentors.notConfirmedFromMentor(userId, false);
            ViewBag.MyMentors = _mentors.getMentors(userId, false);

            return View();
        }

        public async Task<ActionResult> MyTrainee()
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            var userId = user.Id;

            ViewBag.notConfirmedFromTrainee = _mentors.notConfirmedFromTrainee(userId, true);
            ViewBag.notConfirmedFromMentor = _mentors.notConfirmedFromMentor(userId, true);
            ViewBag.MyMentors = _mentors.getMentors(userId, true);

            return View();
        }

        public async Task<ActionResult> FindMentor(string? SearchEmail, string? SearchFamily, string? SearchName)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            var userId = user.Id;

            List<User> allUsers = _userManager.Users.Where(p => p.isActive != false).ToList();
            allUsers.Remove(user);

            //фильтры
            try
            {
                if (SearchEmail != null)
                {
                    allUsers = (List<User>)allUsers.Where(x => x.Email.Contains(SearchEmail)).ToList();
                    ViewBag.SearchEmail = SearchEmail;
                }

                if (SearchFamily != null)
                {
                    allUsers = (List<User>)allUsers.Where(x => x.Surname.Contains(SearchFamily)).ToList();
                    ViewBag.SearchFamily = SearchFamily;
                }

                if (SearchName != null)
                {
                    allUsers = (List<User>)allUsers.Where(x => x.Name.Contains(SearchName)).ToList();
                    ViewBag.SearchName = SearchName;
                }
            }
            catch (Exception)
            {
                ViewBag.SearchEmail = null;
                ViewBag.SearchFamily = null;
                ViewBag.SearchName = null;
            }

            List<AllUsersViewModel> allUsersView = new List<AllUsersViewModel>();

            foreach (var item in allUsers)
            {
                AllUsersViewModel buf = new AllUsersViewModel();
                buf.Surname = item.Surname;
                buf.Name = item.Name;
                buf.Email = item.Email;

                if (_context.mentors.Where(p => p.TraineeId == userId && p.TraineeConfirmed == true && p.MentorId == item.Id).Count() > 0)
                {
                    buf.showMentorButton = false;
                }
                else { buf.showMentorButton = true; }

                if (_context.mentors.Where(p => p.MentorId == userId && p.MentorConfirmed == true && p.TraineeId == item.Id).Count() > 0)
                {
                    buf.showTraineeButton = false;
                }
                else { buf.showTraineeButton = true; }

                allUsersView.Add(buf);
            }

            ViewBag.AllUsers = allUsersView;
            return View();
        }

        public async Task<ActionResult> AddMentor(string Email, bool? search)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            var userId = user.Id;

            var mentorId = _userManager.Users.First(p => p.Email == Email).Id;

            var buf = _context.mentors.Where(p => p.MentorId == mentorId && p.TraineeId == userId);

            //если нет записи
            if (buf.Count() == 0)
            {
                Mentors mentor = new Mentors();
                mentor.MentorId = mentorId;
                mentor.MentorConfirmed = false;
                mentor.TraineeId = userId;
                mentor.TraineeConfirmed = true;

                _context.Add(mentor);
                await _context.SaveChangesAsync();
                if (search!= null)
                {
                    return RedirectToAction("Index", "Search");
                }
                return RedirectToAction("MyMentors", "Mentors");
            }
            //если есть запись
            else if (buf.Count() == 1)
            {
                int s = buf.First().Id;
                var mentor = await _context.mentors.FindAsync(s);

                mentor.TraineeConfirmed = true;

                _context.Update(mentor);
                await _context.SaveChangesAsync();
                if (search != null)
                {
                    return RedirectToAction("Index", "Search");
                }
                return RedirectToAction("MyMentors", "Mentors");
            }

            return RedirectToAction("MyMentors", "Mentors");
        }

        public async Task<ActionResult> RemoveMentor(string Email, bool? search)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            var userId = user.Id;

            var mentorId = _userManager.Users.First(p => p.Email == Email).Id;

            var buf = _context.mentors.Where(p => p.MentorId == mentorId && p.TraineeId == userId);

            //если нет записи
            if (buf.Count() == 0)
            {
                //ничего
                return RedirectToAction("MyMentors", "Mentors");
            }
            //если есть запись
            else if (buf.Count() == 1)
            {
                int s = buf.First().Id;

                var mentor = await _context.mentors.FindAsync(s);

                //удалить наставника
                if (mentor.TraineeConfirmed == true && mentor.MentorConfirmed == true)
                {
                    mentor.TraineeConfirmed = false;
                    _context.Update(mentor);
                    await _context.SaveChangesAsync();
                }
                //удалить запись
                else if (mentor.TraineeConfirmed == true && mentor.MentorConfirmed == false)
                {
                    _context.Remove(mentor);
                    await _context.SaveChangesAsync();
                }

                if (search != null)
                {
                    return RedirectToAction("Index", "Search");
                }

                return RedirectToAction("MyMentors", "Mentors");
            }

            return RedirectToAction("MyMentors", "Mentors");
        }

        public async Task<ActionResult> AddTrainee(string Email, bool? search)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            var userId = user.Id;

            var traineeId = _userManager.Users.First(p => p.Email == Email).Id;

            var buf = _context.mentors.Where(p => p.MentorId == userId && p.TraineeId == traineeId);

            //если нет записи
            if (buf.Count() == 0)
            {
                Mentors mentor = new Mentors();
                mentor.MentorId = userId;
                mentor.MentorConfirmed = true;
                mentor.TraineeId = traineeId;
                mentor.TraineeConfirmed = false;

                _context.Add(mentor);
                await _context.SaveChangesAsync();
                if (search != null)
                {
                    return RedirectToAction("Index", "Search");
                }
                return RedirectToAction("MyTrainee", "Mentors");
            }
            //если есть запись
            else if (buf.Count() == 1)
            {
                int s = buf.First().Id;
                var trainee = await _context.mentors.FindAsync(s);

                trainee.MentorConfirmed = true;

                _context.Update(trainee);
                await _context.SaveChangesAsync();
                if (search != null)
                {
                    return RedirectToAction("Index", "Search");
                }
                return RedirectToAction("MyTrainee", "Mentors");
            }

            return RedirectToAction("MyTrainee", "Mentors");
        }

        public async Task<ActionResult> RemoveTrainee(string Email, bool? search)
        {
            var user = await _userManager.GetUserAsync(User); if (user ==null) return RedirectToAction("Login", "Account");
            var userId = user.Id;

            var traineeId = _userManager.Users.First(p => p.Email == Email).Id;

            var buf = _context.mentors.Where(p => p.MentorId == userId && p.TraineeId == traineeId);

            //если нет записи
            if (buf.Count() == 0)
            {
                //ничего
                return RedirectToAction("MyTrainee", "Mentors");
            }
            //если есть запись
            else if (buf.Count() == 1)
            {
                int s = buf.First().Id;

                var trainee = await _context.mentors.FindAsync(s);

                //удалить подопечного
                if (trainee.TraineeConfirmed == true && trainee.MentorConfirmed == true)
                {
                    trainee.MentorConfirmed = false;
                    _context.Update(trainee);
                    await _context.SaveChangesAsync();
                }
                //удалить запись
                else if (trainee.TraineeConfirmed == false && trainee.MentorConfirmed == true)
                {
                    _context.Remove(trainee);
                    await _context.SaveChangesAsync();
                }
                if (search != null)
                {
                    return RedirectToAction("Index", "Search");
                }

                return RedirectToAction("MyTrainee", "Mentors");
            }

            return RedirectToAction("MyTrainee", "Mentors");
        }
    }
}
