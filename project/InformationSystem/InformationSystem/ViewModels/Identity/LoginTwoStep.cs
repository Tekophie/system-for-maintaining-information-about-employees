﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.ViewModels.Identity
{
    public class LoginTwoStep
    {
        [Required(ErrorMessage = "Обязательное поле")]
        [DataType(DataType.Text)]
        [Display(Name = "Код:")]
        public string TwoFactorCode { get; set; }
        public bool RememberMe { get; set; }

        public string? ReturnUrl { get; set; }
    }
}
