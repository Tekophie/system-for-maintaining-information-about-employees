﻿using InformationSystem.Data;
using InformationSystem.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.ViewModels.Project
{
    public class ProjectRepository : IProject
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public ProjectRepository(UserManager<User> userManager, ApplicationDbContext context)
        {
            _context = context;
            _userManager = userManager;
        }

        public List<ProjectViewModel> GetProject(string userId)
        {
            var ffff = _context.projectList.Where(p => p.EmployeeId == userId && p.ProjectsId != null && p.ProjectsId != null)
                                                         .Include(o => o.projects)
                                                         .Include(o => o.technologiesLists)
                                                         .ToList();

            List<ProjectViewModel> projectViewModel = new List<ProjectViewModel>();
            foreach (var item in ffff)
            {
                projectViewModel.Add(new ProjectViewModel
                {
                    projectList = item,
                    tasks = _context.tasks.Where(p => p.ProjectsId == item.ProjectsId).ToList(),

                    technologiesList = _context.technologiesLists.Where(p => p.ProjectListId == item.ProjectsListId)
                    .Include(p => p.technologies).ToList()
                });
            }
            return projectViewModel;
        }
    }
}
