﻿namespace InformationSystem.ViewModels.Project
{
    public interface IProject
    {
        List<ProjectViewModel> GetProject(string userId);
    }
}
