﻿using InformationSystem.Data.Models.Project;

namespace InformationSystem.ViewModels.Project
{
    public class ProjectViewModel
    {
        public ProjectList projectList { get; set; }

        public List<TechnologiesList> technologiesList { get; set; }

        public List<Tasks> tasks { get; set; }
    }
}
