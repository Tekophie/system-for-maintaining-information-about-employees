﻿using InformationSystem.Data.Models;
using InformationSystem.Data.Models.Project;
using InformationSystem.Data.Models.SkillsAndEducation;

namespace InformationSystem.ViewModels
{
    public class ProfileViewModel
    {
        public User user { get; set; }

        public EducationList educations { get; set; }

        public ProjectList projects { get; set; }
    }
}
