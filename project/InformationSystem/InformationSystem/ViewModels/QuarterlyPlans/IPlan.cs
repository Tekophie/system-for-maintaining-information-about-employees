﻿namespace InformationSystem.ViewModels.QuarterlyPlans
{
    public interface IPlan
    {
        List<PlanViewModel> GetPlan(string userId);
    }
}
