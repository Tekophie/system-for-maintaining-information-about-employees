﻿using InformationSystem.Data.Models.QuarterlyPlans;

namespace InformationSystem.ViewModels.QuarterlyPlans
{
    public class PlansViewModel
    {
        public QuarterlyDevelopmentPlanList quarterlyDevelopmentPlanList { get; set; }
        public QuarterlyDevelopmentPlan quarterlyDevelopmentPlan { get; set; }
        public List<PlanPartsList>? planPartsList { get; set; }

    }
}
