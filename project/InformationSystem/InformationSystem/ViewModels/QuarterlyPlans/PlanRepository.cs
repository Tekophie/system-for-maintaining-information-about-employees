﻿using InformationSystem.Data;
using InformationSystem.Data.Models;
using InformationSystem.ViewModels.QuarterlyPlans;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.ViewModels.QuarterlyPlans
{
    public class PlanRepository : IPlan
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public PlanRepository(UserManager<User> userManager, ApplicationDbContext context)
        {
            _context = context;
            _userManager = userManager;
        }

        public List<PlanViewModel> GetPlan(string userId)
        {
            var ffff = _context.quarterlyDevelopmentPlanList.Where(p => p.EmployeeId == userId && p.QuarterlyDevelopmentPlan != null && p.QuarterlyDevelopmentPlanId != null)
                                                         .Include(o => o.QuarterlyDevelopmentPlan)                                                         
                                                         .ToList();
            
            List<PlanViewModel> planViewModel = new List<PlanViewModel>();
            foreach (var item in ffff)
            {
                planViewModel.Add(new PlanViewModel
                {
                    quarterlyDevelopmentPlanList = item,

                    planPartsList = _context.planPartsList.Where(p => p.PlanId == item.QuarterlyDevelopmentPlanId)
                    .Include(p => p.PlanParts).ToList()
                });
            }
            return planViewModel;
        }
    }
}
