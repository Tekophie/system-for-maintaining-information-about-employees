﻿using InformationSystem.Data.Models.QuarterlyPlans;

namespace InformationSystem.ViewModels.QuarterlyPlans
{
    public class PlanViewModel
    {
        public QuarterlyDevelopmentPlanList quarterlyDevelopmentPlanList { get; set; }
        public List<PlanPartsList>? planPartsList { get; set; }

    }
}
