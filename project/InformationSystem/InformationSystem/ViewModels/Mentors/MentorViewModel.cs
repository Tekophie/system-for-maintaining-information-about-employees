﻿namespace InformationSystem.ViewModels.Mentors
{
    public class MentorViewModel
    {
        //public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }
    }
}
