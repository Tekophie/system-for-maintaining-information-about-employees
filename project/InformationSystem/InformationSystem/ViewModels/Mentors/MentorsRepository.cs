﻿using InformationSystem.Data;
using InformationSystem.Data.Models;
using Microsoft.AspNetCore.Identity;

namespace InformationSystem.ViewModels.Mentors
{
    public class MentorsRepository : IMentors
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public MentorsRepository(UserManager<User> userManager, ApplicationDbContext context)
        {
            _context = context;
            _userManager = userManager;
        }

        public IEnumerable<MentorViewModel> getMentors(string userId, bool isMentor)
        {
            var obj = null as dynamic;
            IEnumerable<MentorViewModel> mentorViewModels = new List<MentorViewModel>();

            if (isMentor)
            {
                obj = _context.mentors.Where(p => p.MentorId == userId && p.MentorConfirmed == true && p.TraineeConfirmed == true);
                mentorViewModels = getTraineeViewModel(obj);
            }
            else
            {
                obj = _context.mentors.Where(p => p.TraineeId == userId && p.MentorConfirmed == true && p.TraineeConfirmed == true);
                mentorViewModels = getMenterViewModel(obj);
            }

            return mentorViewModels;
        }

        public IEnumerable<MentorViewModel> notConfirmedFromTrainee(string userId, bool isMentor)
        {
            var obj = null as dynamic;
            IEnumerable<MentorViewModel> mentorViewModels = new List<MentorViewModel>();

            if (isMentor)
            {
                obj = _context.mentors.Where(p => p.MentorId == userId && p.MentorConfirmed == true && p.TraineeConfirmed == false);
                mentorViewModels = getTraineeViewModel(obj);
            }
            else
            {
                obj = _context.mentors.Where(p => p.TraineeId == userId && p.MentorConfirmed == true && p.TraineeConfirmed == false);
                mentorViewModels = getMenterViewModel(obj);
            }

            return mentorViewModels;
        }

        public IEnumerable<MentorViewModel> notConfirmedFromMentor(string userId, bool isMentor)
        {
            var obj = null as dynamic;
            IEnumerable<MentorViewModel> mentorViewModels = new List<MentorViewModel>();

            if (isMentor)
            {
                obj = _context.mentors.Where(p => p.MentorId == userId && p.MentorConfirmed == false && p.TraineeConfirmed == true);
                mentorViewModels = getTraineeViewModel(obj);
            }
            else
            {
                obj = _context.mentors.Where(p => p.TraineeId == userId && p.MentorConfirmed == false && p.TraineeConfirmed == true);
                mentorViewModels = getMenterViewModel(obj);
            }

            return mentorViewModels;
        }


        public IEnumerable<MentorViewModel> getMenterViewModel(dynamic obj)
        {
            List<MentorViewModel> list = new List<MentorViewModel>();

            var allUsers = _userManager.Users.ToList();

            foreach (var item in obj)
            {
                MentorViewModel buf = new MentorViewModel();
                //buf.Id = item.Id;

                var mentor = allUsers.FirstOrDefault(p => p.Id == item.MentorId);
                buf.Surname = mentor.Surname;
                buf.Name = mentor.Name;
                buf.Email = mentor.Email;
                list.Add(buf);
            }
            return list;
        }

        public IEnumerable<MentorViewModel> getTraineeViewModel(dynamic obj)
        {
            List<MentorViewModel> list = new List<MentorViewModel>();

            var allUsers = _userManager.Users.ToList();

            foreach (var item in obj)
            {
                MentorViewModel buf = new MentorViewModel();
                //buf.Id = item.Id;

                var mentor = allUsers.FirstOrDefault(p => p.Id == item.TraineeId);
                buf.Surname = mentor.Surname;
                buf.Name = mentor.Name;
                buf.Email = mentor.Email;
                list.Add(buf);
            }
            return list;
        }
    }


}
