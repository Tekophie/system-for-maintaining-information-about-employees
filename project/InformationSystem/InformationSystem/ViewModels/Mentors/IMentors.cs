﻿namespace InformationSystem.ViewModels.Mentors
{
    public interface IMentors
    {
        IEnumerable<MentorViewModel> getMentors(string userId, bool isMentor);

        IEnumerable<MentorViewModel> notConfirmedFromTrainee(string userId, bool isMentor);

        IEnumerable<MentorViewModel> notConfirmedFromMentor(string userId, bool isMentor);
    }
}
