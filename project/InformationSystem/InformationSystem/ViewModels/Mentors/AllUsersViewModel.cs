﻿namespace InformationSystem.ViewModels.Mentors
{
    public class AllUsersViewModel
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public bool? showMentorButton { get; set; }
        public bool? showTraineeButton { get; set; }
        public bool? showProfileButton { get; set; }
    }
}
