﻿using InformationSystem.Data.Models.SkillsAndEducation;

namespace InformationSystem.ViewModels.Skills
{
    public class CreateCourseViewModel
    {
        public Education education { get; set; }

        public List<HardSkills> hardSkills { get; set; }

        public List<HardCompetenciesList> hardCompetenciesList { get; set; }

        public List<SoftSkills> softSkills { get; set; }

        public List<SoftCompetenciesList> softCompetenciesList { get; set; }

        public EducationList educationList { get; set; }
    }
}
