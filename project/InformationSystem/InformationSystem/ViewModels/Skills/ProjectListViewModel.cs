﻿using InformationSystem.Data.Models.Project;

namespace InformationSystem.ViewModels.Skills
{
    public class ProjectListViewModel
    {
        public ProjectList projectList { get; set; }

        public List<TechnologiesList>? technologiesLists { get; set; }

    }
}
