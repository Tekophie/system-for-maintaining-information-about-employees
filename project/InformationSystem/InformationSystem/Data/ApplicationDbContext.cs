﻿using InformationSystem.Data.Models;
using InformationSystem.Data.Models.Project;
using InformationSystem.Data.Models.QuarterlyPlans;
using InformationSystem.Data.Models.SkillsAndEducation;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace InformationSystem.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        //HardSkills
        public DbSet<HardSkills> hardSkills { get; set; }
        public DbSet<HardSkillsList> hardSkillsList { get; set; }
        public DbSet<HardCompetenciesList> hardCompetenciesList { get; set; }

        //SoftSkills
        public DbSet<SoftSkills> softSkills { get; set; }
        public DbSet<SoftSkillsList> softSkillsList { get; set; }
        public DbSet<SoftCompetenciesList> softCompetenciesList { get; set; }
        public DbSet<CompetenceLevel> competenceLevel { get; set; }
        public DbSet<CompetencyCategories> competencyCategories { get; set; }

        //Education
        public DbSet<Education> education { get; set; }
        public DbSet<EducationList> educationList { get; set; }

        //QuarterPlan
        public DbSet<QuarterlyDevelopmentPlan> quarterlyDevelopmentPlan { get; set; }
        public DbSet<QuarterlyDevelopmentPlanList> quarterlyDevelopmentPlanList { get; set; }
        public DbSet<PlanParts> planParts { get; set; }
        public DbSet<PlanPartsList> planPartsList { get; set; }

        //projects
        public DbSet<ProjectList> projectList { get; set; }
        public DbSet<Projects> projects { get; set; }
        public DbSet<Tasks> tasks { get; set; }
        public DbSet<Technologies> technologies { get; set; }
        public DbSet<TechnologiesList> technologiesLists { get; set; }

        //наставники
        public DbSet<Mentors> mentors { get; set; }

        //логи
        public DbSet<Logging> logging { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<QuarterlyDevelopmentPlanList>().HasKey(QD => new { QD.EmployeeId, QD.PlanId });
            //modelBuilder.Entity<PlanPartsList>().HasKey(PP => new { PP.PlanPartsId, PP.PlanId });

            //ролли пользователей
            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole
                {
                    Id = "ca8d9346-02c6-4c88-be51-712d58bc467c",
                    Name = "Admin",
                    NormalizedName = "ADMIN",
                    ConcurrencyStamp = "bf9eeb04-3cdf-4436-86d8-a07c8d00a3cf"
                },
                new IdentityRole
                {
                    Id = "c9508e97-818f-4268-bd8c-794d422fb7d9",
                    Name = "Manager",
                    NormalizedName = "MANAGER",
                    ConcurrencyStamp = "69baf7e3-9314-4354-95e7-b9623cfa4d6f"
                }
                );



            //добавление пользователя
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = "4a45fcf1-90c8-4f46-b5cd-16bbfabb9a20",
                    Name = "fsf1",
                    Surname = "max1",
                    UserName = "dkmakarkin@edu.hse.ru",
                    NormalizedUserName = "DKMAKARKIN@EDU.HSE.RU",
                    Email = "dkmakarkin@edu.hse.ru",
                    NormalizedEmail = "DKMAKARKIN@EDU.HSE.RU",
                    EmailConfirmed = true,
                    PasswordHash = "AQAAAAEAACcQAAAAEBHFQBBxigJmZTbkHtgEaUA8hEV36f1F5Xx388spRMsir8F0bESJqzFISq1Td58SyA==",
                    SecurityStamp = "SV7RKMHL2VO5764WYJ4KHTA55EVFINMG",
                    ConcurrencyStamp = "e2f5bf8e-35c8-4733-9b33-a6054b1443c9",
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                },
                new User
                {
                    Id = "4a45fcf1-90c8-4f46-b5cd-16bbfabb9a22",
                    Name = "fsf2",
                    Surname = "max2",
                    UserName = "dkmakarki@edu.hse.ru",
                    NormalizedUserName = "DKMAKARKI@EDU.HSE.RU",
                    Email = "dkmakarki@edu.hse.ru",
                    NormalizedEmail = "DKMAKARKI@EDU.HSE.RU",
                    EmailConfirmed = true,
                    PasswordHash = "AQAAAAEAACcQAAAAEBHFQBBxigJmZTbkHtgEaUA8hEV36f1F5Xx388spRMsir8F0bESJqzFISq1Td58SyA==",
                    SecurityStamp = "SV7RKMHL2VO5764WYJ4KHTA55EVFINMG",
                    ConcurrencyStamp = "e2f5bf8e-35c8-4733-9b33-a6054b1443c9",
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                },
                new User
                {
                    Id = "4a45fcf1-90c8-4f46-b5cd-16bbfabb9a24",
                    Name = "fsf3",
                    Surname = "max3",
                    UserName = "dkmakark@edu.hse.ru",
                    NormalizedUserName = "DKMAKARK@EDU.HSE.RU",
                    Email = "dkmakark@edu.hse.ru",
                    NormalizedEmail = "DKMAKARK@EDU.HSE.RU",
                    EmailConfirmed = true,
                    PasswordHash = "AQAAAAEAACcQAAAAEBHFQBBxigJmZTbkHtgEaUA8hEV36f1F5Xx388spRMsir8F0bESJqzFISq1Td58SyA==",
                    SecurityStamp = "SV7RKMHL2VO5764WYJ4KHTA55EVFINMG",
                    ConcurrencyStamp = "e2f5bf8e-35c8-4733-9b33-a6054b1443c9",
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                },
                new User
                {
                    Id = "cad9ccee-6cc8-42c0-bb8b-23a7bc5a3c74",
                    Name = "123",
                    Surname = "123",
                    UserName = "brenichev@yandex.ru",
                    NormalizedUserName = "BRENICHEV@YANDEX.RU",
                    Email = "brenichev@yandex.ru",
                    NormalizedEmail = "BRENICHEV@YANDEX.RU",
                    EmailConfirmed = true,
                    PasswordHash = "AQAAAAEAACcQAAAAEALQTU20JjDtwQGvahkCsSWASD9Km44oPFbJuci3i4us9119H++/kVcCoydwVDm4zw==",
                    SecurityStamp = "QFIAVQXBOCE7TCHD7XMLU4PUR5GPKREX",
                    ConcurrencyStamp = "dc5447eb-9895-4396-9c07-396ff3db4f31",
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                },
                //Логин: admin@test.ru Пароль: %z7N~0%M9J
                new User
                {
                    Id = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d",
                    Name = "admin",
                    Surname = "admin",
                    UserName = "admin@test.ru",
                    NormalizedUserName = "ADMIN@TEST.RU",
                    Email = "admin@test.ru",
                    NormalizedEmail = "ADMIN@TEST.RU",
                    EmailConfirmed = true,
                    PasswordHash = "AQAAAAEAACcQAAAAEKlY/BkGUsMpvzeu0CNe7rtaMKWvYkPvBPp1u0wkxPzqK5bYmTD/tYAuzGnGrGubhg==",
                    SecurityStamp = "3R6IHVXYCM2WWU45XZV73HKFOTV4FP7X",
                    ConcurrencyStamp = "f8671b44-9d54-458b-8943-2bd3577f8ca8",
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                }
                );


            //присвоение пользователю ролей
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>
                {
                    RoleId = "ca8d9346-02c6-4c88-be51-712d58bc467c",
                    UserId = "4a45fcf1-90c8-4f46-b5cd-16bbfabb9a20"
                },
                new IdentityUserRole<string>
                {
                    RoleId = "c9508e97-818f-4268-bd8c-794d422fb7d9",
                    UserId = "4a45fcf1-90c8-4f46-b5cd-16bbfabb9a22"
                },
                new IdentityUserRole<string>
                {
                    RoleId = "ca8d9346-02c6-4c88-be51-712d58bc467c",
                    UserId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d"
                }
            );

            //уровни компетенций
            modelBuilder.Entity<CompetenceLevel>().HasData(
                new CompetenceLevel { CompetenceLevelId = 1, Level = "1 lvl" },
                new CompetenceLevel { CompetenceLevelId = 2, Level = "2 lvl" },
                new CompetenceLevel { CompetenceLevelId = 3, Level = "3 lvl" },
                new CompetenceLevel { CompetenceLevelId = 4, Level = "4 lvl" },
                new CompetenceLevel { CompetenceLevelId = 5, Level = "5 lvl" });

            //категории компетенций
            modelBuilder.Entity<CompetencyCategories>().HasData(
                new CompetencyCategories { CompetencyCategoriesId = 1, CompetencyCategory = "Язык программирования" },
                new CompetencyCategories { CompetencyCategoriesId = 2, CompetencyCategory = "Коммуникации" });


            //hardSkills
            modelBuilder.Entity<HardSkills>().HasData(
                new HardSkills
                {
                    HardSkillsId = 1,
                    CompetencyCategoriesId = 1,
                    CompetencyName = "C#",
                    CompetencyDescription = "язык C#"
                },
                new HardSkills
                {
                    HardSkillsId = 2,
                    CompetencyCategoriesId = 1,
                    CompetencyName = "C++",
                    CompetencyDescription = "язык C++"
                },
                new HardSkills
                {
                    HardSkillsId = 3,
                    CompetencyCategoriesId = 1,
                    CompetencyName = "Go",
                    CompetencyDescription = "язык Go"
                },
                new HardSkills
                {
                    HardSkillsId = 4,
                    CompetencyCategoriesId = 1,
                    CompetencyName = "Java",
                    CompetencyDescription = "язык Java"
                },
                new HardSkills
                {
                    HardSkillsId = 5,
                    CompetencyCategoriesId = 1,
                    CompetencyName = "JavaScript",
                    CompetencyDescription = "язык JavaScript"
                }
                );


            //softSkills
            modelBuilder.Entity<SoftSkills>().HasData(
                new SoftSkills { SoftSkillsId = 1, CompetencyName = "Работа в команде" },
                new SoftSkills { SoftSkillsId = 2, CompetencyName = "Лидерство" },
                new SoftSkills { SoftSkillsId = 3, CompetencyName = "Пунктуальность" },
                new SoftSkills { SoftSkillsId = 4, CompetencyName = "Чувство юмора" },
                new SoftSkills { SoftSkillsId = 5, CompetencyName = "Обучаемость" }
                );


            //technologies
            modelBuilder.Entity<Technologies>().HasData(
                new Technologies { TechnologiesId = 1, TechnologiesName = ".Net" },
                new Technologies { TechnologiesId = 2, TechnologiesName = "CMS" },
                new Technologies { TechnologiesId = 3, TechnologiesName = "Spring" },
                new Technologies { TechnologiesId = 4, TechnologiesName = "Node.js" },
                new Technologies { TechnologiesId = 5, TechnologiesName = "Node.js 2.0" }
                );

            //courses
            modelBuilder.Entity<Education>().HasData(
               new Education { EducationId = 1, CourseName = "Python Junior Developer" },
               new Education { EducationId = 2, CourseName = "Java Junior Developer" }

               );

            //courseHardCompetence
            modelBuilder.Entity<HardCompetenciesList>().HasData(
               new HardCompetenciesList { HardCompetenciesListId = 1, EducationId = 1, HardSkillsId = 1, CompetenceLevelId = 1 },
               new HardCompetenciesList { HardCompetenciesListId = 2, EducationId = 2, HardSkillsId = 3, CompetenceLevelId = 1 }

               );

            //courseSoftCompetence
            modelBuilder.Entity<SoftCompetenciesList>().HasData(
               new SoftCompetenciesList { SoftCompetenciesListId = 1, EducationId = 1, SoftSkillsId = 2, CompetenceLevelId = 1 },
               new SoftCompetenciesList { SoftCompetenciesListId = 2, EducationId = 2, SoftSkillsId = 3, CompetenceLevelId = 1 }

               );
            //usersCourses
            modelBuilder.Entity<EducationList>().HasData(
               new EducationList
               {
                   EducationListId = 1,
                   EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d",
                   EducationId = 1,
                   PassageDateStart = Convert.ToDateTime("2023-03-01 23:45:00"),
                   PassageDateFinish = Convert.ToDateTime("2023-03-02 23:45:00"),
                   Certificate = "сертификат",
                   Comment = "коммент"
               },
               new EducationList
               {
                   EducationListId = 2,
                   EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d",
                   EducationId = 2,
                   PassageDateStart = Convert.ToDateTime("2023-03-01 23:45:00"),
                   PassageDateFinish = Convert.ToDateTime("2023-03-02 23:45:00"),
                   Certificate = "сертификат",
                   Comment = "коммент"
               },
               new EducationList
               {
                   EducationListId = 3,
                   EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d",
                   EducationId = 2,
                   PassageDateStart = Convert.ToDateTime("2023-02-01 23:45:00"),
                   PassageDateFinish = Convert.ToDateTime("2023-02-02 23:45:00"),
                   Certificate = "сертификат",
                   Comment = "коммент"
               }
               );


            //usersHardSkills
            modelBuilder.Entity<HardSkillsList>().HasData(
               new HardSkillsList { HardSkillListId = 1, EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d", HardSkillsId = 1, CompetenceLevelId = 1 },
               new HardSkillsList { HardSkillListId = 2, EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d", HardSkillsId = 2, CompetenceLevelId = 2 }
               );

            //usersSoftSkills
            modelBuilder.Entity<SoftSkillsList>().HasData(
               new SoftSkillsList { SoftSkillsListId = 1, EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d", SoftSkillsId = 1, CompetenceLevelId = 1 },
               new SoftSkillsList { SoftSkillsListId = 2, EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d", SoftSkillsId = 2, CompetenceLevelId = 2 }
               );


            //projects
            modelBuilder.Entity<Projects>().HasData(
               new Projects { Id = 1, Name = "ИАСУ", Description = "описание проекта ИАСУ", UserId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d" },
               new Projects { Id = 2, Name = "ИАСУП", Description = "описание проекта ИАСУП", UserId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d" }
               );

            //projectsTasks
            modelBuilder.Entity<Tasks>().HasData(
               new Tasks { TasksId = 1, ProjectsId = 1, TaskName = "нарисовать диаграмму" },
               new Tasks { TasksId = 2, ProjectsId = 1, TaskName = "Спроектировать БД" },
               new Tasks { TasksId = 3, ProjectsId = 2, TaskName = "нарисовать диаграмму ИАСУП" },
               new Tasks { TasksId = 4, ProjectsId = 2, TaskName = "нарисовать диаграмму ИАСУП" }
               );

            //usersProjects
            modelBuilder.Entity<ProjectList>().HasData(
               new ProjectList
               {
                   ProjectsListId = 1,
                   EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d",
                   ProjectsId = 1,
                   StartDate = Convert.ToDateTime("2023-02-02 23:45:00"),
                   EndDate = Convert.ToDateTime("2023-02-02 23:45:00")
               },
               new ProjectList
               {
                   ProjectsListId = 2,
                   EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d",
                   ProjectsId = 2,
                   StartDate = Convert.ToDateTime("2023-02-02 23:45:00"),
                   EndDate = Convert.ToDateTime("2023-02-02 23:45:00")
               }
               );

            //projectTechnologies
            modelBuilder.Entity<TechnologiesList>().HasData(
               new TechnologiesList { TechnologiesListId = 1, EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d", ProjectListId = 1, TechnologiesId = 1 },
               new TechnologiesList { TechnologiesListId = 2, EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d", ProjectListId = 1, TechnologiesId = 3 },
               new TechnologiesList { TechnologiesListId = 3, EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d", ProjectListId = 1, TechnologiesId = 4 },
               new TechnologiesList { TechnologiesListId = 4, EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d", ProjectListId = 2, TechnologiesId = 2 },
               new TechnologiesList { TechnologiesListId = 5, EmployeeId = "4a2d490f-f1fc-4ecc-b7f9-9f95dba87a8d", ProjectListId = 2, TechnologiesId = 5 }
               );
        }
    }
}