﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.Project
{
    public class Tasks
    {
        [Key]
        public int TasksId { get; set; }

        [Display(Name = "Проект")]
        [Required]
        public int ProjectsId { get; set; }
        public Projects? Projects { get; set; }

        [Display(Name = "Задача")]
        [Required]
        public string TaskName { get; set; }
    }
}
