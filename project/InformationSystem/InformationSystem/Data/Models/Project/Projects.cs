﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.Project
{
    public class Projects
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Название проекта")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        public string? Description { get; set; }

        [Display(Name = "Руководитель проекта")]
        public string UserId { get; set; }
        public User? User { get; set; }
    }
}
