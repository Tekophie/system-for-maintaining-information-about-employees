﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.Project
{
    public class TechnologiesList
    {
        [Key]
        public int TechnologiesListId { get; set; }

        public int? ProjectListId { get; set; }
        public ProjectList? projectList { get; set; }

        [Required]
        public string EmployeeId { get; set; }

        [Display(Name = "Технология")]
        [Required]
        public int? TechnologiesId { get; set; }
        public Technologies? technologies { get; set; }
    }
}
