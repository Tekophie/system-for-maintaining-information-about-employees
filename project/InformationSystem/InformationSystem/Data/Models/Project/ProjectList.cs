﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.Project
{
    public class ProjectList
    {
        [Key]
        public int ProjectsListId { get; set; }

        [Required]
        public string EmployeeId { get; set; }

        [Display(Name = "Проект")]
        //[Required]
        public int? ProjectsId { get; set; }
        public Projects? projects { get; set; }

        [Display(Name = "Дата начала")]
        //[Required]
        public DateTime? StartDate { get; set; }

        [Display(Name = "Дата окончания")]
        //[Required]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Технологии")]
        public int? TechnologiesListId { get; set; }
        public List<TechnologiesList>? technologiesLists { get; set; }
    }
}
