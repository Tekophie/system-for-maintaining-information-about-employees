﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.Project
{
    public class Technologies
    {
        [Key]
        public int TechnologiesId { get; set; }

        [Display(Name = "Название технологии")]
        [Required]
        public string TechnologiesName { get; set; }

        public List<TechnologiesList>? technologiesLists { get; set; }

    }
}
