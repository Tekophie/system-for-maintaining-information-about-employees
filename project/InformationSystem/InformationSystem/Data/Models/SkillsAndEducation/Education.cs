﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.SkillsAndEducation
{
    public class Education
    {
        [Key]
        public int EducationId { get; set; }

        [Required]
        [Display(Name = "Наименование курса")]
        public string CourseName { get; set; }

        [Display(Name = "Профессиональные навыки")]
        public int? HardCompetenciesListId { get; set; }

        [Display(Name = "Личностные качества")]
        public int? SoftCompetenciesListId { get; set; }

        //public List<HardCompetenciesList>? HardCompetenciesList { get; set; }
    }
}
