﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.SkillsAndEducation
{
    public class HardSkills
    {
        [Key]
        public int HardSkillsId { get; set; }

        [Display(Name = "Категория компетенции")]
        [Required]
        public int CompetencyCategoriesId { get; set; }
        public CompetencyCategories? CompetencyCategories { get; set; }

        [Display(Name = "Наименование")]
        [Required]
        public string CompetencyName { get; set; }

        [Display(Name = "Описание")]
        public string? CompetencyDescription { get; set; }

        public List<HardCompetenciesList>? HardCompetenciesList { get; set; }
    }
}
