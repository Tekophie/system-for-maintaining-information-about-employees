﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.SkillsAndEducation
{
    public class HardSkillsList
    {
        [Key]
        public int HardSkillListId { get; set; }

        public string EmployeeId { get; set; }

        [Display(Name = "Навык")]
        public int HardSkillsId { get; set; }
        public HardSkills? hardSkill { get; set; }

        [Display(Name = "Уровень компетентности")]
        [Required]
        public int CompetenceLevelId { get; set; }
        public CompetenceLevel? CompetencеLevel { get; set; }

        [Display(Name = "Отношение к компетентности")]
        public string? AttitudeToCompetence { get; set; }

        [Display(Name = "Желание развиваться")]
        public string? DesireToDevelop { get; set; }
    }
}
