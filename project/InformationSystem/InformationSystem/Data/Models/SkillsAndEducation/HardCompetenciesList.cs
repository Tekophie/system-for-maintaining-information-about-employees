﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.SkillsAndEducation
{
    public class HardCompetenciesList
    {
        [Key]
        public int HardCompetenciesListId { get; set; }

        public int EducationId { get; set; }

        [Display(Name = "Навык")]
        public int HardSkillsId { get; set; }
        public HardSkills? HardSkills { get; set; }

        public int CompetenceLevelId { get; set; }
        public CompetenceLevel? CompetenceLevel { get; set; }
    }
}
