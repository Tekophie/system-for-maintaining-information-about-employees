﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.SkillsAndEducation
{
    public class EducationList
    {
        [Key]
        public int EducationListId { get; set; }

        public string EmployeeId { get; set; }

        [Display(Name = "Курс")]
        [Required]
        public int EducationId { get; set; }
        public Education? education { get; set; }

        [Display(Name = "Дата начала")]
        public DateTime PassageDateStart { get; set; }

        [Display(Name = "Дата окончания")]
        public DateTime PassageDateFinish { get; set; }

        [Display(Name = "Сертификат")]
        public string? Certificate { get; set; }

        [Display(Name = "Комментарий")]
        public string? Comment { get; set; }

    }
}
