﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.SkillsAndEducation
{
    public class CompetencyCategories
    {
        [Key]
        public int CompetencyCategoriesId { get; set; }

        [Display(Name = "Категория навыка")]
        public string CompetencyCategory { get; set; }

        public List<HardSkillsList>? HardSkillsList { get; set; }
    }
}
