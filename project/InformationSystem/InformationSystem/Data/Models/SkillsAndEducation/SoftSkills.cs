﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.SkillsAndEducation
{
    public class SoftSkills
    {
        [Key]
        public int SoftSkillsId { get; set; }

        [Display(Name = "Личностное качество")]
        [Required]
        public string CompetencyName { get; set; }

        [Display(Name = "Описание")]
        public string? CompetencyDescriprion { get; set; }

        public List<SoftSkillsList>? SoftCompetenciesList { get; set; }
    }
}
