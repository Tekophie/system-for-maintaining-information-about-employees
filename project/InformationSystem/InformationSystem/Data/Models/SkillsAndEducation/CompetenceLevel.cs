﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.SkillsAndEducation
{
    public class CompetenceLevel
    {
        [Key]
        public int CompetenceLevelId { get; set; }

        [Display(Name = "Уровень компетентности")]
        public string Level { get; set; }

        public List<HardSkills>? HardSkills { get; set; }

        public List<HardCompetenciesList>? HardCompetenciesList { get; set; }

        public List<SoftSkillsList>? SoftCompetenciesList { get; set; }
    }
}
