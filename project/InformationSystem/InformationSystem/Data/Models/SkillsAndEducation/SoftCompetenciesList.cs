﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.SkillsAndEducation
{
    public class SoftCompetenciesList
    {
        [Key]
        public int SoftCompetenciesListId { get; set; }

        public int EducationId { get; set; }

        [Display(Name = "Качество")]
        public int SoftSkillsId { get; set; }
        public SoftSkills? SoftSkills { get; set; }

        public int CompetenceLevelId { get; set; }
        public CompetenceLevel? CompetenceLevel { get; set; }
    }
}
