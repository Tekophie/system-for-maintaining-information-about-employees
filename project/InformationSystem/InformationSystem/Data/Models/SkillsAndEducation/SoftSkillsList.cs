﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.SkillsAndEducation
{
    public class SoftSkillsList
    {
        [Key]
        public int SoftSkillsListId { get; set; }

        public string EmployeeId { get; set; }

        [Display(Name = "Качество")]
        [Required]
        public int SoftSkillsId { get; set; }
        public SoftSkills? SoftSkills { get; set; }

        [Display(Name = "Уровень компетенции")]
        [Required]
        public int CompetenceLevelId { get; set; }
        public CompetenceLevel? CompetencеLevel { get; set; }

        [Display(Name = "Желание развиваться")]
        public string? DesireToDevelop { get; set; }
    }
}
