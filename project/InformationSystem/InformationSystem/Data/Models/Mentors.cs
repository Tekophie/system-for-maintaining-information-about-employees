﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models
{
    public class Mentors
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string MentorId { get; set; }
        public bool MentorConfirmed { get; set; }

        [Required]
        public string TraineeId { get; set; }
        public bool TraineeConfirmed { get; set; }
    }
}
