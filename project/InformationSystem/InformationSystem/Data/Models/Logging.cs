﻿
using Microsoft.Build.Framework;

namespace InformationSystem.Data.Models
{
    public class Logging
    {
        public int Id { get; set; }
        [Required]
        public string EmployeeId { get; set; }

        public DateTime Date { get; set; }

        public string Description { get; set; }
    }
}
