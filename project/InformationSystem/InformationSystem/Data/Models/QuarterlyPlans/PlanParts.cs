﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.QuarterlyPlans
{
    public class PlanParts
    {
        [Key]
        public int PlanPartsId { get; set; }

        [Display(Name = "Направление")]
        public string? ActivityDirection { get; set; }

        [Required]
        [Display(Name = "Измеряемый результат")]
        public string? MeasurableResult { get; set; }

        [Display(Name = "Deadline")]
        public DateTime? Deadline { get; set; }

        [Display(Name = "Оценка в часах")]
        public string? RatingInHours { get; set; }

        [Display(Name = "Текущий прогресс")]
        public string? CurrentProgress { get; set; }

        public string UserId { get; set; }

        public List<QuarterlyDevelopmentPlan>? QuarterlyDevelopmentPlan { get; set; }
    }
}
