﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.QuarterlyPlans
{
    public class QuarterlyDevelopmentPlan
    {
        [Key]
        public int PlanId { get; set; }

        [Display(Name = "Квартал")]
        public string Quarter { get; set; }

        [Required]
        [Display(Name = "Цель")]
        public string? Objective { get; set; }

        [Display(Name = "Критерии достижения")]
        public string? AchievementCriteria { get; set; }

        [Display(Name = "Польза для сотрудника")]
        public string? BenefitsForEmployee { get; set; }

        [Display(Name = "Польза для компании")]
        public string? BenefitsForCompany { get; set; }

        [Display(Name = "Обсуждения")]
        public string? Discuss { get; set; }

        [Display(Name = "Дата обсуждения")]
        public DateTime DiscussDate { get; set; }

        public string UserId { get; set; }

        public List<QuarterlyDevelopmentPlanList>? QuarterlyDevelopmentPlanList { get; set; }
    }
}
