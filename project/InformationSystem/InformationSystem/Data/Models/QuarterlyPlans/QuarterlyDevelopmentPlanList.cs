﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.QuarterlyPlans
{
    public class QuarterlyDevelopmentPlanList
    {
        [Key]
        public int PlansId { get; set; }

        [Display(Name = "Сотрудник")]
        public string EmployeeId { get; set; }

        //public User? User { get; set; }

        [Display(Name = "Квартальный план")]
        public int? QuarterlyDevelopmentPlanId { get; set; }

        public QuarterlyDevelopmentPlan? QuarterlyDevelopmentPlan { get; set; }

    }
}
