﻿using System.ComponentModel.DataAnnotations;

namespace InformationSystem.Data.Models.QuarterlyPlans
{
    public class PlanPartsList
    {
        [Key]
        public int PlanPartsListId { get; set; }

        [Display(Name = "Подплан")]
        public int PlanPartsId { get; set; }

        public PlanParts? PlanParts { get; set; }

        [Display(Name = "Квартальный план")]
        public int PlanId { get; set; }

        public QuarterlyDevelopmentPlan? QuarterlyDevelopmentPlan { get; set; }
    }
}
