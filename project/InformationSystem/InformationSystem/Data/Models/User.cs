﻿using InformationSystem.Data.Models.QuarterlyPlans;
using Microsoft.AspNetCore.Identity;


namespace InformationSystem.Data.Models
{
    public class User : IdentityUser
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string? Petronymic { get; set; }

        public int? MentorlID { get; set; }

        public bool? isActive { get; set; }

        public DateTime? DateOfAttestation { get; set; }

        public List<QuarterlyDevelopmentPlanList> QuarterlyDevelopmentPlanList { get; set; }
    }
}
